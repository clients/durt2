class Input$GeolocInput {
  factory Input$GeolocInput({
    required double latitude,
    required double longitude,
  }) =>
      Input$GeolocInput._({
        r'latitude': latitude,
        r'longitude': longitude,
      });

  Input$GeolocInput._(this._$data);

  factory Input$GeolocInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$latitude = data['latitude'];
    result$data['latitude'] = (l$latitude as num).toDouble();
    final l$longitude = data['longitude'];
    result$data['longitude'] = (l$longitude as num).toDouble();
    return Input$GeolocInput._(result$data);
  }

  Map<String, dynamic> _$data;

  double get latitude => (_$data['latitude'] as double);

  double get longitude => (_$data['longitude'] as double);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$latitude = latitude;
    result$data['latitude'] = l$latitude;
    final l$longitude = longitude;
    result$data['longitude'] = l$longitude;
    return result$data;
  }

  CopyWith$Input$GeolocInput<Input$GeolocInput> get copyWith =>
      CopyWith$Input$GeolocInput(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$GeolocInput) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$latitude = latitude;
    final lOther$latitude = other.latitude;
    if (l$latitude != lOther$latitude) {
      return false;
    }
    final l$longitude = longitude;
    final lOther$longitude = other.longitude;
    if (l$longitude != lOther$longitude) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$latitude = latitude;
    final l$longitude = longitude;
    return Object.hashAll([
      l$latitude,
      l$longitude,
    ]);
  }
}

abstract class CopyWith$Input$GeolocInput<TRes> {
  factory CopyWith$Input$GeolocInput(
    Input$GeolocInput instance,
    TRes Function(Input$GeolocInput) then,
  ) = _CopyWithImpl$Input$GeolocInput;

  factory CopyWith$Input$GeolocInput.stub(TRes res) =
      _CopyWithStubImpl$Input$GeolocInput;

  TRes call({
    double? latitude,
    double? longitude,
  });
}

class _CopyWithImpl$Input$GeolocInput<TRes>
    implements CopyWith$Input$GeolocInput<TRes> {
  _CopyWithImpl$Input$GeolocInput(
    this._instance,
    this._then,
  );

  final Input$GeolocInput _instance;

  final TRes Function(Input$GeolocInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? latitude = _undefined,
    Object? longitude = _undefined,
  }) =>
      _then(Input$GeolocInput._({
        ..._instance._$data,
        if (latitude != _undefined && latitude != null)
          'latitude': (latitude as double),
        if (longitude != _undefined && longitude != null)
          'longitude': (longitude as double),
      }));
}

class _CopyWithStubImpl$Input$GeolocInput<TRes>
    implements CopyWith$Input$GeolocInput<TRes> {
  _CopyWithStubImpl$Input$GeolocInput(this._res);

  TRes _res;

  call({
    double? latitude,
    double? longitude,
  }) =>
      _res;
}

class Input$jsonb_cast_exp {
  factory Input$jsonb_cast_exp({Input$String_comparison_exp? $String}) =>
      Input$jsonb_cast_exp._({
        if ($String != null) r'String': $String,
      });

  Input$jsonb_cast_exp._(this._$data);

  factory Input$jsonb_cast_exp.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    if (data.containsKey('String')) {
      final l$$String = data['String'];
      result$data['String'] = l$$String == null
          ? null
          : Input$String_comparison_exp.fromJson(
              (l$$String as Map<String, dynamic>));
    }
    return Input$jsonb_cast_exp._(result$data);
  }

  Map<String, dynamic> _$data;

  Input$String_comparison_exp? get $String =>
      (_$data['String'] as Input$String_comparison_exp?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    if (_$data.containsKey('String')) {
      final l$$String = $String;
      result$data['String'] = l$$String?.toJson();
    }
    return result$data;
  }

  CopyWith$Input$jsonb_cast_exp<Input$jsonb_cast_exp> get copyWith =>
      CopyWith$Input$jsonb_cast_exp(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$jsonb_cast_exp) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$$String = $String;
    final lOther$$String = other.$String;
    if (_$data.containsKey('String') != other._$data.containsKey('String')) {
      return false;
    }
    if (l$$String != lOther$$String) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$$String = $String;
    return Object.hashAll(
        [_$data.containsKey('String') ? l$$String : const {}]);
  }
}

abstract class CopyWith$Input$jsonb_cast_exp<TRes> {
  factory CopyWith$Input$jsonb_cast_exp(
    Input$jsonb_cast_exp instance,
    TRes Function(Input$jsonb_cast_exp) then,
  ) = _CopyWithImpl$Input$jsonb_cast_exp;

  factory CopyWith$Input$jsonb_cast_exp.stub(TRes res) =
      _CopyWithStubImpl$Input$jsonb_cast_exp;

  TRes call({Input$String_comparison_exp? $String});
  CopyWith$Input$String_comparison_exp<TRes> get $String;
}

class _CopyWithImpl$Input$jsonb_cast_exp<TRes>
    implements CopyWith$Input$jsonb_cast_exp<TRes> {
  _CopyWithImpl$Input$jsonb_cast_exp(
    this._instance,
    this._then,
  );

  final Input$jsonb_cast_exp _instance;

  final TRes Function(Input$jsonb_cast_exp) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({Object? $String = _undefined}) => _then(Input$jsonb_cast_exp._({
        ..._instance._$data,
        if ($String != _undefined)
          'String': ($String as Input$String_comparison_exp?),
      }));

  CopyWith$Input$String_comparison_exp<TRes> get $String {
    final local$$String = _instance.$String;
    return local$$String == null
        ? CopyWith$Input$String_comparison_exp.stub(_then(_instance))
        : CopyWith$Input$String_comparison_exp(
            local$$String, (e) => call($String: e));
  }
}

class _CopyWithStubImpl$Input$jsonb_cast_exp<TRes>
    implements CopyWith$Input$jsonb_cast_exp<TRes> {
  _CopyWithStubImpl$Input$jsonb_cast_exp(this._res);

  TRes _res;

  call({Input$String_comparison_exp? $String}) => _res;

  CopyWith$Input$String_comparison_exp<TRes> get $String =>
      CopyWith$Input$String_comparison_exp.stub(_res);
}

class Input$jsonb_comparison_exp {
  factory Input$jsonb_comparison_exp({
    Input$jsonb_cast_exp? $_cast,
    Map<String, dynamic>? $_contained_in,
    Map<String, dynamic>? $_contains,
    Map<String, dynamic>? $_eq,
    Map<String, dynamic>? $_gt,
    Map<String, dynamic>? $_gte,
    String? $_has_key,
    List<String>? $_has_keys_all,
    List<String>? $_has_keys_any,
    List<Map<String, dynamic>>? $_in,
    bool? $_is_null,
    Map<String, dynamic>? $_lt,
    Map<String, dynamic>? $_lte,
    Map<String, dynamic>? $_neq,
    List<Map<String, dynamic>>? $_nin,
  }) =>
      Input$jsonb_comparison_exp._({
        if ($_cast != null) r'_cast': $_cast,
        if ($_contained_in != null) r'_contained_in': $_contained_in,
        if ($_contains != null) r'_contains': $_contains,
        if ($_eq != null) r'_eq': $_eq,
        if ($_gt != null) r'_gt': $_gt,
        if ($_gte != null) r'_gte': $_gte,
        if ($_has_key != null) r'_has_key': $_has_key,
        if ($_has_keys_all != null) r'_has_keys_all': $_has_keys_all,
        if ($_has_keys_any != null) r'_has_keys_any': $_has_keys_any,
        if ($_in != null) r'_in': $_in,
        if ($_is_null != null) r'_is_null': $_is_null,
        if ($_lt != null) r'_lt': $_lt,
        if ($_lte != null) r'_lte': $_lte,
        if ($_neq != null) r'_neq': $_neq,
        if ($_nin != null) r'_nin': $_nin,
      });

  Input$jsonb_comparison_exp._(this._$data);

  factory Input$jsonb_comparison_exp.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    if (data.containsKey('_cast')) {
      final l$$_cast = data['_cast'];
      result$data['_cast'] = l$$_cast == null
          ? null
          : Input$jsonb_cast_exp.fromJson((l$$_cast as Map<String, dynamic>));
    }
    if (data.containsKey('_contained_in')) {
      final l$$_contained_in = data['_contained_in'];
      result$data['_contained_in'] =
          (l$$_contained_in as Map<String, dynamic>?);
    }
    if (data.containsKey('_contains')) {
      final l$$_contains = data['_contains'];
      result$data['_contains'] = (l$$_contains as Map<String, dynamic>?);
    }
    if (data.containsKey('_eq')) {
      final l$$_eq = data['_eq'];
      result$data['_eq'] = (l$$_eq as Map<String, dynamic>?);
    }
    if (data.containsKey('_gt')) {
      final l$$_gt = data['_gt'];
      result$data['_gt'] = (l$$_gt as Map<String, dynamic>?);
    }
    if (data.containsKey('_gte')) {
      final l$$_gte = data['_gte'];
      result$data['_gte'] = (l$$_gte as Map<String, dynamic>?);
    }
    if (data.containsKey('_has_key')) {
      final l$$_has_key = data['_has_key'];
      result$data['_has_key'] = (l$$_has_key as String?);
    }
    if (data.containsKey('_has_keys_all')) {
      final l$$_has_keys_all = data['_has_keys_all'];
      result$data['_has_keys_all'] = (l$$_has_keys_all as List<dynamic>?)
          ?.map((e) => (e as String))
          .toList();
    }
    if (data.containsKey('_has_keys_any')) {
      final l$$_has_keys_any = data['_has_keys_any'];
      result$data['_has_keys_any'] = (l$$_has_keys_any as List<dynamic>?)
          ?.map((e) => (e as String))
          .toList();
    }
    if (data.containsKey('_in')) {
      final l$$_in = data['_in'];
      result$data['_in'] = (l$$_in as List<dynamic>?)
          ?.map((e) => (e as Map<String, dynamic>))
          .toList();
    }
    if (data.containsKey('_is_null')) {
      final l$$_is_null = data['_is_null'];
      result$data['_is_null'] = (l$$_is_null as bool?);
    }
    if (data.containsKey('_lt')) {
      final l$$_lt = data['_lt'];
      result$data['_lt'] = (l$$_lt as Map<String, dynamic>?);
    }
    if (data.containsKey('_lte')) {
      final l$$_lte = data['_lte'];
      result$data['_lte'] = (l$$_lte as Map<String, dynamic>?);
    }
    if (data.containsKey('_neq')) {
      final l$$_neq = data['_neq'];
      result$data['_neq'] = (l$$_neq as Map<String, dynamic>?);
    }
    if (data.containsKey('_nin')) {
      final l$$_nin = data['_nin'];
      result$data['_nin'] = (l$$_nin as List<dynamic>?)
          ?.map((e) => (e as Map<String, dynamic>))
          .toList();
    }
    return Input$jsonb_comparison_exp._(result$data);
  }

  Map<String, dynamic> _$data;

  Input$jsonb_cast_exp? get $_cast =>
      (_$data['_cast'] as Input$jsonb_cast_exp?);

  Map<String, dynamic>? get $_contained_in =>
      (_$data['_contained_in'] as Map<String, dynamic>?);

  Map<String, dynamic>? get $_contains =>
      (_$data['_contains'] as Map<String, dynamic>?);

  Map<String, dynamic>? get $_eq => (_$data['_eq'] as Map<String, dynamic>?);

  Map<String, dynamic>? get $_gt => (_$data['_gt'] as Map<String, dynamic>?);

  Map<String, dynamic>? get $_gte => (_$data['_gte'] as Map<String, dynamic>?);

  String? get $_has_key => (_$data['_has_key'] as String?);

  List<String>? get $_has_keys_all =>
      (_$data['_has_keys_all'] as List<String>?);

  List<String>? get $_has_keys_any =>
      (_$data['_has_keys_any'] as List<String>?);

  List<Map<String, dynamic>>? get $_in =>
      (_$data['_in'] as List<Map<String, dynamic>>?);

  bool? get $_is_null => (_$data['_is_null'] as bool?);

  Map<String, dynamic>? get $_lt => (_$data['_lt'] as Map<String, dynamic>?);

  Map<String, dynamic>? get $_lte => (_$data['_lte'] as Map<String, dynamic>?);

  Map<String, dynamic>? get $_neq => (_$data['_neq'] as Map<String, dynamic>?);

  List<Map<String, dynamic>>? get $_nin =>
      (_$data['_nin'] as List<Map<String, dynamic>>?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    if (_$data.containsKey('_cast')) {
      final l$$_cast = $_cast;
      result$data['_cast'] = l$$_cast?.toJson();
    }
    if (_$data.containsKey('_contained_in')) {
      final l$$_contained_in = $_contained_in;
      result$data['_contained_in'] = l$$_contained_in;
    }
    if (_$data.containsKey('_contains')) {
      final l$$_contains = $_contains;
      result$data['_contains'] = l$$_contains;
    }
    if (_$data.containsKey('_eq')) {
      final l$$_eq = $_eq;
      result$data['_eq'] = l$$_eq;
    }
    if (_$data.containsKey('_gt')) {
      final l$$_gt = $_gt;
      result$data['_gt'] = l$$_gt;
    }
    if (_$data.containsKey('_gte')) {
      final l$$_gte = $_gte;
      result$data['_gte'] = l$$_gte;
    }
    if (_$data.containsKey('_has_key')) {
      final l$$_has_key = $_has_key;
      result$data['_has_key'] = l$$_has_key;
    }
    if (_$data.containsKey('_has_keys_all')) {
      final l$$_has_keys_all = $_has_keys_all;
      result$data['_has_keys_all'] = l$$_has_keys_all?.map((e) => e).toList();
    }
    if (_$data.containsKey('_has_keys_any')) {
      final l$$_has_keys_any = $_has_keys_any;
      result$data['_has_keys_any'] = l$$_has_keys_any?.map((e) => e).toList();
    }
    if (_$data.containsKey('_in')) {
      final l$$_in = $_in;
      result$data['_in'] = l$$_in?.map((e) => e).toList();
    }
    if (_$data.containsKey('_is_null')) {
      final l$$_is_null = $_is_null;
      result$data['_is_null'] = l$$_is_null;
    }
    if (_$data.containsKey('_lt')) {
      final l$$_lt = $_lt;
      result$data['_lt'] = l$$_lt;
    }
    if (_$data.containsKey('_lte')) {
      final l$$_lte = $_lte;
      result$data['_lte'] = l$$_lte;
    }
    if (_$data.containsKey('_neq')) {
      final l$$_neq = $_neq;
      result$data['_neq'] = l$$_neq;
    }
    if (_$data.containsKey('_nin')) {
      final l$$_nin = $_nin;
      result$data['_nin'] = l$$_nin?.map((e) => e).toList();
    }
    return result$data;
  }

  CopyWith$Input$jsonb_comparison_exp<Input$jsonb_comparison_exp>
      get copyWith => CopyWith$Input$jsonb_comparison_exp(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$jsonb_comparison_exp) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$$_cast = $_cast;
    final lOther$$_cast = other.$_cast;
    if (_$data.containsKey('_cast') != other._$data.containsKey('_cast')) {
      return false;
    }
    if (l$$_cast != lOther$$_cast) {
      return false;
    }
    final l$$_contained_in = $_contained_in;
    final lOther$$_contained_in = other.$_contained_in;
    if (_$data.containsKey('_contained_in') !=
        other._$data.containsKey('_contained_in')) {
      return false;
    }
    if (l$$_contained_in != lOther$$_contained_in) {
      return false;
    }
    final l$$_contains = $_contains;
    final lOther$$_contains = other.$_contains;
    if (_$data.containsKey('_contains') !=
        other._$data.containsKey('_contains')) {
      return false;
    }
    if (l$$_contains != lOther$$_contains) {
      return false;
    }
    final l$$_eq = $_eq;
    final lOther$$_eq = other.$_eq;
    if (_$data.containsKey('_eq') != other._$data.containsKey('_eq')) {
      return false;
    }
    if (l$$_eq != lOther$$_eq) {
      return false;
    }
    final l$$_gt = $_gt;
    final lOther$$_gt = other.$_gt;
    if (_$data.containsKey('_gt') != other._$data.containsKey('_gt')) {
      return false;
    }
    if (l$$_gt != lOther$$_gt) {
      return false;
    }
    final l$$_gte = $_gte;
    final lOther$$_gte = other.$_gte;
    if (_$data.containsKey('_gte') != other._$data.containsKey('_gte')) {
      return false;
    }
    if (l$$_gte != lOther$$_gte) {
      return false;
    }
    final l$$_has_key = $_has_key;
    final lOther$$_has_key = other.$_has_key;
    if (_$data.containsKey('_has_key') !=
        other._$data.containsKey('_has_key')) {
      return false;
    }
    if (l$$_has_key != lOther$$_has_key) {
      return false;
    }
    final l$$_has_keys_all = $_has_keys_all;
    final lOther$$_has_keys_all = other.$_has_keys_all;
    if (_$data.containsKey('_has_keys_all') !=
        other._$data.containsKey('_has_keys_all')) {
      return false;
    }
    if (l$$_has_keys_all != null && lOther$$_has_keys_all != null) {
      if (l$$_has_keys_all.length != lOther$$_has_keys_all.length) {
        return false;
      }
      for (int i = 0; i < l$$_has_keys_all.length; i++) {
        final l$$_has_keys_all$entry = l$$_has_keys_all[i];
        final lOther$$_has_keys_all$entry = lOther$$_has_keys_all[i];
        if (l$$_has_keys_all$entry != lOther$$_has_keys_all$entry) {
          return false;
        }
      }
    } else if (l$$_has_keys_all != lOther$$_has_keys_all) {
      return false;
    }
    final l$$_has_keys_any = $_has_keys_any;
    final lOther$$_has_keys_any = other.$_has_keys_any;
    if (_$data.containsKey('_has_keys_any') !=
        other._$data.containsKey('_has_keys_any')) {
      return false;
    }
    if (l$$_has_keys_any != null && lOther$$_has_keys_any != null) {
      if (l$$_has_keys_any.length != lOther$$_has_keys_any.length) {
        return false;
      }
      for (int i = 0; i < l$$_has_keys_any.length; i++) {
        final l$$_has_keys_any$entry = l$$_has_keys_any[i];
        final lOther$$_has_keys_any$entry = lOther$$_has_keys_any[i];
        if (l$$_has_keys_any$entry != lOther$$_has_keys_any$entry) {
          return false;
        }
      }
    } else if (l$$_has_keys_any != lOther$$_has_keys_any) {
      return false;
    }
    final l$$_in = $_in;
    final lOther$$_in = other.$_in;
    if (_$data.containsKey('_in') != other._$data.containsKey('_in')) {
      return false;
    }
    if (l$$_in != null && lOther$$_in != null) {
      if (l$$_in.length != lOther$$_in.length) {
        return false;
      }
      for (int i = 0; i < l$$_in.length; i++) {
        final l$$_in$entry = l$$_in[i];
        final lOther$$_in$entry = lOther$$_in[i];
        if (l$$_in$entry != lOther$$_in$entry) {
          return false;
        }
      }
    } else if (l$$_in != lOther$$_in) {
      return false;
    }
    final l$$_is_null = $_is_null;
    final lOther$$_is_null = other.$_is_null;
    if (_$data.containsKey('_is_null') !=
        other._$data.containsKey('_is_null')) {
      return false;
    }
    if (l$$_is_null != lOther$$_is_null) {
      return false;
    }
    final l$$_lt = $_lt;
    final lOther$$_lt = other.$_lt;
    if (_$data.containsKey('_lt') != other._$data.containsKey('_lt')) {
      return false;
    }
    if (l$$_lt != lOther$$_lt) {
      return false;
    }
    final l$$_lte = $_lte;
    final lOther$$_lte = other.$_lte;
    if (_$data.containsKey('_lte') != other._$data.containsKey('_lte')) {
      return false;
    }
    if (l$$_lte != lOther$$_lte) {
      return false;
    }
    final l$$_neq = $_neq;
    final lOther$$_neq = other.$_neq;
    if (_$data.containsKey('_neq') != other._$data.containsKey('_neq')) {
      return false;
    }
    if (l$$_neq != lOther$$_neq) {
      return false;
    }
    final l$$_nin = $_nin;
    final lOther$$_nin = other.$_nin;
    if (_$data.containsKey('_nin') != other._$data.containsKey('_nin')) {
      return false;
    }
    if (l$$_nin != null && lOther$$_nin != null) {
      if (l$$_nin.length != lOther$$_nin.length) {
        return false;
      }
      for (int i = 0; i < l$$_nin.length; i++) {
        final l$$_nin$entry = l$$_nin[i];
        final lOther$$_nin$entry = lOther$$_nin[i];
        if (l$$_nin$entry != lOther$$_nin$entry) {
          return false;
        }
      }
    } else if (l$$_nin != lOther$$_nin) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$$_cast = $_cast;
    final l$$_contained_in = $_contained_in;
    final l$$_contains = $_contains;
    final l$$_eq = $_eq;
    final l$$_gt = $_gt;
    final l$$_gte = $_gte;
    final l$$_has_key = $_has_key;
    final l$$_has_keys_all = $_has_keys_all;
    final l$$_has_keys_any = $_has_keys_any;
    final l$$_in = $_in;
    final l$$_is_null = $_is_null;
    final l$$_lt = $_lt;
    final l$$_lte = $_lte;
    final l$$_neq = $_neq;
    final l$$_nin = $_nin;
    return Object.hashAll([
      _$data.containsKey('_cast') ? l$$_cast : const {},
      _$data.containsKey('_contained_in') ? l$$_contained_in : const {},
      _$data.containsKey('_contains') ? l$$_contains : const {},
      _$data.containsKey('_eq') ? l$$_eq : const {},
      _$data.containsKey('_gt') ? l$$_gt : const {},
      _$data.containsKey('_gte') ? l$$_gte : const {},
      _$data.containsKey('_has_key') ? l$$_has_key : const {},
      _$data.containsKey('_has_keys_all')
          ? l$$_has_keys_all == null
              ? null
              : Object.hashAll(l$$_has_keys_all.map((v) => v))
          : const {},
      _$data.containsKey('_has_keys_any')
          ? l$$_has_keys_any == null
              ? null
              : Object.hashAll(l$$_has_keys_any.map((v) => v))
          : const {},
      _$data.containsKey('_in')
          ? l$$_in == null
              ? null
              : Object.hashAll(l$$_in.map((v) => v))
          : const {},
      _$data.containsKey('_is_null') ? l$$_is_null : const {},
      _$data.containsKey('_lt') ? l$$_lt : const {},
      _$data.containsKey('_lte') ? l$$_lte : const {},
      _$data.containsKey('_neq') ? l$$_neq : const {},
      _$data.containsKey('_nin')
          ? l$$_nin == null
              ? null
              : Object.hashAll(l$$_nin.map((v) => v))
          : const {},
    ]);
  }
}

abstract class CopyWith$Input$jsonb_comparison_exp<TRes> {
  factory CopyWith$Input$jsonb_comparison_exp(
    Input$jsonb_comparison_exp instance,
    TRes Function(Input$jsonb_comparison_exp) then,
  ) = _CopyWithImpl$Input$jsonb_comparison_exp;

  factory CopyWith$Input$jsonb_comparison_exp.stub(TRes res) =
      _CopyWithStubImpl$Input$jsonb_comparison_exp;

  TRes call({
    Input$jsonb_cast_exp? $_cast,
    Map<String, dynamic>? $_contained_in,
    Map<String, dynamic>? $_contains,
    Map<String, dynamic>? $_eq,
    Map<String, dynamic>? $_gt,
    Map<String, dynamic>? $_gte,
    String? $_has_key,
    List<String>? $_has_keys_all,
    List<String>? $_has_keys_any,
    List<Map<String, dynamic>>? $_in,
    bool? $_is_null,
    Map<String, dynamic>? $_lt,
    Map<String, dynamic>? $_lte,
    Map<String, dynamic>? $_neq,
    List<Map<String, dynamic>>? $_nin,
  });
  CopyWith$Input$jsonb_cast_exp<TRes> get $_cast;
}

class _CopyWithImpl$Input$jsonb_comparison_exp<TRes>
    implements CopyWith$Input$jsonb_comparison_exp<TRes> {
  _CopyWithImpl$Input$jsonb_comparison_exp(
    this._instance,
    this._then,
  );

  final Input$jsonb_comparison_exp _instance;

  final TRes Function(Input$jsonb_comparison_exp) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? $_cast = _undefined,
    Object? $_contained_in = _undefined,
    Object? $_contains = _undefined,
    Object? $_eq = _undefined,
    Object? $_gt = _undefined,
    Object? $_gte = _undefined,
    Object? $_has_key = _undefined,
    Object? $_has_keys_all = _undefined,
    Object? $_has_keys_any = _undefined,
    Object? $_in = _undefined,
    Object? $_is_null = _undefined,
    Object? $_lt = _undefined,
    Object? $_lte = _undefined,
    Object? $_neq = _undefined,
    Object? $_nin = _undefined,
  }) =>
      _then(Input$jsonb_comparison_exp._({
        ..._instance._$data,
        if ($_cast != _undefined) '_cast': ($_cast as Input$jsonb_cast_exp?),
        if ($_contained_in != _undefined)
          '_contained_in': ($_contained_in as Map<String, dynamic>?),
        if ($_contains != _undefined)
          '_contains': ($_contains as Map<String, dynamic>?),
        if ($_eq != _undefined) '_eq': ($_eq as Map<String, dynamic>?),
        if ($_gt != _undefined) '_gt': ($_gt as Map<String, dynamic>?),
        if ($_gte != _undefined) '_gte': ($_gte as Map<String, dynamic>?),
        if ($_has_key != _undefined) '_has_key': ($_has_key as String?),
        if ($_has_keys_all != _undefined)
          '_has_keys_all': ($_has_keys_all as List<String>?),
        if ($_has_keys_any != _undefined)
          '_has_keys_any': ($_has_keys_any as List<String>?),
        if ($_in != _undefined) '_in': ($_in as List<Map<String, dynamic>>?),
        if ($_is_null != _undefined) '_is_null': ($_is_null as bool?),
        if ($_lt != _undefined) '_lt': ($_lt as Map<String, dynamic>?),
        if ($_lte != _undefined) '_lte': ($_lte as Map<String, dynamic>?),
        if ($_neq != _undefined) '_neq': ($_neq as Map<String, dynamic>?),
        if ($_nin != _undefined) '_nin': ($_nin as List<Map<String, dynamic>>?),
      }));

  CopyWith$Input$jsonb_cast_exp<TRes> get $_cast {
    final local$$_cast = _instance.$_cast;
    return local$$_cast == null
        ? CopyWith$Input$jsonb_cast_exp.stub(_then(_instance))
        : CopyWith$Input$jsonb_cast_exp(local$$_cast, (e) => call($_cast: e));
  }
}

class _CopyWithStubImpl$Input$jsonb_comparison_exp<TRes>
    implements CopyWith$Input$jsonb_comparison_exp<TRes> {
  _CopyWithStubImpl$Input$jsonb_comparison_exp(this._res);

  TRes _res;

  call({
    Input$jsonb_cast_exp? $_cast,
    Map<String, dynamic>? $_contained_in,
    Map<String, dynamic>? $_contains,
    Map<String, dynamic>? $_eq,
    Map<String, dynamic>? $_gt,
    Map<String, dynamic>? $_gte,
    String? $_has_key,
    List<String>? $_has_keys_all,
    List<String>? $_has_keys_any,
    List<Map<String, dynamic>>? $_in,
    bool? $_is_null,
    Map<String, dynamic>? $_lt,
    Map<String, dynamic>? $_lte,
    Map<String, dynamic>? $_neq,
    List<Map<String, dynamic>>? $_nin,
  }) =>
      _res;

  CopyWith$Input$jsonb_cast_exp<TRes> get $_cast =>
      CopyWith$Input$jsonb_cast_exp.stub(_res);
}

class Input$point_comparison_exp {
  factory Input$point_comparison_exp({
    String? $_eq,
    String? $_gt,
    String? $_gte,
    List<String>? $_in,
    bool? $_is_null,
    String? $_lt,
    String? $_lte,
    String? $_neq,
    List<String>? $_nin,
  }) =>
      Input$point_comparison_exp._({
        if ($_eq != null) r'_eq': $_eq,
        if ($_gt != null) r'_gt': $_gt,
        if ($_gte != null) r'_gte': $_gte,
        if ($_in != null) r'_in': $_in,
        if ($_is_null != null) r'_is_null': $_is_null,
        if ($_lt != null) r'_lt': $_lt,
        if ($_lte != null) r'_lte': $_lte,
        if ($_neq != null) r'_neq': $_neq,
        if ($_nin != null) r'_nin': $_nin,
      });

  Input$point_comparison_exp._(this._$data);

  factory Input$point_comparison_exp.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    if (data.containsKey('_eq')) {
      final l$$_eq = data['_eq'];
      result$data['_eq'] = (l$$_eq as String?);
    }
    if (data.containsKey('_gt')) {
      final l$$_gt = data['_gt'];
      result$data['_gt'] = (l$$_gt as String?);
    }
    if (data.containsKey('_gte')) {
      final l$$_gte = data['_gte'];
      result$data['_gte'] = (l$$_gte as String?);
    }
    if (data.containsKey('_in')) {
      final l$$_in = data['_in'];
      result$data['_in'] =
          (l$$_in as List<dynamic>?)?.map((e) => (e as String)).toList();
    }
    if (data.containsKey('_is_null')) {
      final l$$_is_null = data['_is_null'];
      result$data['_is_null'] = (l$$_is_null as bool?);
    }
    if (data.containsKey('_lt')) {
      final l$$_lt = data['_lt'];
      result$data['_lt'] = (l$$_lt as String?);
    }
    if (data.containsKey('_lte')) {
      final l$$_lte = data['_lte'];
      result$data['_lte'] = (l$$_lte as String?);
    }
    if (data.containsKey('_neq')) {
      final l$$_neq = data['_neq'];
      result$data['_neq'] = (l$$_neq as String?);
    }
    if (data.containsKey('_nin')) {
      final l$$_nin = data['_nin'];
      result$data['_nin'] =
          (l$$_nin as List<dynamic>?)?.map((e) => (e as String)).toList();
    }
    return Input$point_comparison_exp._(result$data);
  }

  Map<String, dynamic> _$data;

  String? get $_eq => (_$data['_eq'] as String?);

  String? get $_gt => (_$data['_gt'] as String?);

  String? get $_gte => (_$data['_gte'] as String?);

  List<String>? get $_in => (_$data['_in'] as List<String>?);

  bool? get $_is_null => (_$data['_is_null'] as bool?);

  String? get $_lt => (_$data['_lt'] as String?);

  String? get $_lte => (_$data['_lte'] as String?);

  String? get $_neq => (_$data['_neq'] as String?);

  List<String>? get $_nin => (_$data['_nin'] as List<String>?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    if (_$data.containsKey('_eq')) {
      final l$$_eq = $_eq;
      result$data['_eq'] = l$$_eq;
    }
    if (_$data.containsKey('_gt')) {
      final l$$_gt = $_gt;
      result$data['_gt'] = l$$_gt;
    }
    if (_$data.containsKey('_gte')) {
      final l$$_gte = $_gte;
      result$data['_gte'] = l$$_gte;
    }
    if (_$data.containsKey('_in')) {
      final l$$_in = $_in;
      result$data['_in'] = l$$_in?.map((e) => e).toList();
    }
    if (_$data.containsKey('_is_null')) {
      final l$$_is_null = $_is_null;
      result$data['_is_null'] = l$$_is_null;
    }
    if (_$data.containsKey('_lt')) {
      final l$$_lt = $_lt;
      result$data['_lt'] = l$$_lt;
    }
    if (_$data.containsKey('_lte')) {
      final l$$_lte = $_lte;
      result$data['_lte'] = l$$_lte;
    }
    if (_$data.containsKey('_neq')) {
      final l$$_neq = $_neq;
      result$data['_neq'] = l$$_neq;
    }
    if (_$data.containsKey('_nin')) {
      final l$$_nin = $_nin;
      result$data['_nin'] = l$$_nin?.map((e) => e).toList();
    }
    return result$data;
  }

  CopyWith$Input$point_comparison_exp<Input$point_comparison_exp>
      get copyWith => CopyWith$Input$point_comparison_exp(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$point_comparison_exp) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$$_eq = $_eq;
    final lOther$$_eq = other.$_eq;
    if (_$data.containsKey('_eq') != other._$data.containsKey('_eq')) {
      return false;
    }
    if (l$$_eq != lOther$$_eq) {
      return false;
    }
    final l$$_gt = $_gt;
    final lOther$$_gt = other.$_gt;
    if (_$data.containsKey('_gt') != other._$data.containsKey('_gt')) {
      return false;
    }
    if (l$$_gt != lOther$$_gt) {
      return false;
    }
    final l$$_gte = $_gte;
    final lOther$$_gte = other.$_gte;
    if (_$data.containsKey('_gte') != other._$data.containsKey('_gte')) {
      return false;
    }
    if (l$$_gte != lOther$$_gte) {
      return false;
    }
    final l$$_in = $_in;
    final lOther$$_in = other.$_in;
    if (_$data.containsKey('_in') != other._$data.containsKey('_in')) {
      return false;
    }
    if (l$$_in != null && lOther$$_in != null) {
      if (l$$_in.length != lOther$$_in.length) {
        return false;
      }
      for (int i = 0; i < l$$_in.length; i++) {
        final l$$_in$entry = l$$_in[i];
        final lOther$$_in$entry = lOther$$_in[i];
        if (l$$_in$entry != lOther$$_in$entry) {
          return false;
        }
      }
    } else if (l$$_in != lOther$$_in) {
      return false;
    }
    final l$$_is_null = $_is_null;
    final lOther$$_is_null = other.$_is_null;
    if (_$data.containsKey('_is_null') !=
        other._$data.containsKey('_is_null')) {
      return false;
    }
    if (l$$_is_null != lOther$$_is_null) {
      return false;
    }
    final l$$_lt = $_lt;
    final lOther$$_lt = other.$_lt;
    if (_$data.containsKey('_lt') != other._$data.containsKey('_lt')) {
      return false;
    }
    if (l$$_lt != lOther$$_lt) {
      return false;
    }
    final l$$_lte = $_lte;
    final lOther$$_lte = other.$_lte;
    if (_$data.containsKey('_lte') != other._$data.containsKey('_lte')) {
      return false;
    }
    if (l$$_lte != lOther$$_lte) {
      return false;
    }
    final l$$_neq = $_neq;
    final lOther$$_neq = other.$_neq;
    if (_$data.containsKey('_neq') != other._$data.containsKey('_neq')) {
      return false;
    }
    if (l$$_neq != lOther$$_neq) {
      return false;
    }
    final l$$_nin = $_nin;
    final lOther$$_nin = other.$_nin;
    if (_$data.containsKey('_nin') != other._$data.containsKey('_nin')) {
      return false;
    }
    if (l$$_nin != null && lOther$$_nin != null) {
      if (l$$_nin.length != lOther$$_nin.length) {
        return false;
      }
      for (int i = 0; i < l$$_nin.length; i++) {
        final l$$_nin$entry = l$$_nin[i];
        final lOther$$_nin$entry = lOther$$_nin[i];
        if (l$$_nin$entry != lOther$$_nin$entry) {
          return false;
        }
      }
    } else if (l$$_nin != lOther$$_nin) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$$_eq = $_eq;
    final l$$_gt = $_gt;
    final l$$_gte = $_gte;
    final l$$_in = $_in;
    final l$$_is_null = $_is_null;
    final l$$_lt = $_lt;
    final l$$_lte = $_lte;
    final l$$_neq = $_neq;
    final l$$_nin = $_nin;
    return Object.hashAll([
      _$data.containsKey('_eq') ? l$$_eq : const {},
      _$data.containsKey('_gt') ? l$$_gt : const {},
      _$data.containsKey('_gte') ? l$$_gte : const {},
      _$data.containsKey('_in')
          ? l$$_in == null
              ? null
              : Object.hashAll(l$$_in.map((v) => v))
          : const {},
      _$data.containsKey('_is_null') ? l$$_is_null : const {},
      _$data.containsKey('_lt') ? l$$_lt : const {},
      _$data.containsKey('_lte') ? l$$_lte : const {},
      _$data.containsKey('_neq') ? l$$_neq : const {},
      _$data.containsKey('_nin')
          ? l$$_nin == null
              ? null
              : Object.hashAll(l$$_nin.map((v) => v))
          : const {},
    ]);
  }
}

abstract class CopyWith$Input$point_comparison_exp<TRes> {
  factory CopyWith$Input$point_comparison_exp(
    Input$point_comparison_exp instance,
    TRes Function(Input$point_comparison_exp) then,
  ) = _CopyWithImpl$Input$point_comparison_exp;

  factory CopyWith$Input$point_comparison_exp.stub(TRes res) =
      _CopyWithStubImpl$Input$point_comparison_exp;

  TRes call({
    String? $_eq,
    String? $_gt,
    String? $_gte,
    List<String>? $_in,
    bool? $_is_null,
    String? $_lt,
    String? $_lte,
    String? $_neq,
    List<String>? $_nin,
  });
}

class _CopyWithImpl$Input$point_comparison_exp<TRes>
    implements CopyWith$Input$point_comparison_exp<TRes> {
  _CopyWithImpl$Input$point_comparison_exp(
    this._instance,
    this._then,
  );

  final Input$point_comparison_exp _instance;

  final TRes Function(Input$point_comparison_exp) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? $_eq = _undefined,
    Object? $_gt = _undefined,
    Object? $_gte = _undefined,
    Object? $_in = _undefined,
    Object? $_is_null = _undefined,
    Object? $_lt = _undefined,
    Object? $_lte = _undefined,
    Object? $_neq = _undefined,
    Object? $_nin = _undefined,
  }) =>
      _then(Input$point_comparison_exp._({
        ..._instance._$data,
        if ($_eq != _undefined) '_eq': ($_eq as String?),
        if ($_gt != _undefined) '_gt': ($_gt as String?),
        if ($_gte != _undefined) '_gte': ($_gte as String?),
        if ($_in != _undefined) '_in': ($_in as List<String>?),
        if ($_is_null != _undefined) '_is_null': ($_is_null as bool?),
        if ($_lt != _undefined) '_lt': ($_lt as String?),
        if ($_lte != _undefined) '_lte': ($_lte as String?),
        if ($_neq != _undefined) '_neq': ($_neq as String?),
        if ($_nin != _undefined) '_nin': ($_nin as List<String>?),
      }));
}

class _CopyWithStubImpl$Input$point_comparison_exp<TRes>
    implements CopyWith$Input$point_comparison_exp<TRes> {
  _CopyWithStubImpl$Input$point_comparison_exp(this._res);

  TRes _res;

  call({
    String? $_eq,
    String? $_gt,
    String? $_gte,
    List<String>? $_in,
    bool? $_is_null,
    String? $_lt,
    String? $_lte,
    String? $_neq,
    List<String>? $_nin,
  }) =>
      _res;
}

class Input$profiles_bool_exp {
  factory Input$profiles_bool_exp({
    List<Input$profiles_bool_exp>? $_and,
    Input$profiles_bool_exp? $_not,
    List<Input$profiles_bool_exp>? $_or,
    Input$String_comparison_exp? avatar,
    Input$String_comparison_exp? city,
    Input$String_comparison_exp? data_cid,
    Input$String_comparison_exp? description,
    Input$point_comparison_exp? geoloc,
    Input$String_comparison_exp? index_request_cid,
    Input$String_comparison_exp? pubkey,
    Input$jsonb_comparison_exp? socials,
    Input$timestamptz_comparison_exp? time,
    Input$String_comparison_exp? title,
  }) =>
      Input$profiles_bool_exp._({
        if ($_and != null) r'_and': $_and,
        if ($_not != null) r'_not': $_not,
        if ($_or != null) r'_or': $_or,
        if (avatar != null) r'avatar': avatar,
        if (city != null) r'city': city,
        if (data_cid != null) r'data_cid': data_cid,
        if (description != null) r'description': description,
        if (geoloc != null) r'geoloc': geoloc,
        if (index_request_cid != null) r'index_request_cid': index_request_cid,
        if (pubkey != null) r'pubkey': pubkey,
        if (socials != null) r'socials': socials,
        if (time != null) r'time': time,
        if (title != null) r'title': title,
      });

  Input$profiles_bool_exp._(this._$data);

  factory Input$profiles_bool_exp.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    if (data.containsKey('_and')) {
      final l$$_and = data['_and'];
      result$data['_and'] = (l$$_and as List<dynamic>?)
          ?.map((e) =>
              Input$profiles_bool_exp.fromJson((e as Map<String, dynamic>)))
          .toList();
    }
    if (data.containsKey('_not')) {
      final l$$_not = data['_not'];
      result$data['_not'] = l$$_not == null
          ? null
          : Input$profiles_bool_exp.fromJson((l$$_not as Map<String, dynamic>));
    }
    if (data.containsKey('_or')) {
      final l$$_or = data['_or'];
      result$data['_or'] = (l$$_or as List<dynamic>?)
          ?.map((e) =>
              Input$profiles_bool_exp.fromJson((e as Map<String, dynamic>)))
          .toList();
    }
    if (data.containsKey('avatar')) {
      final l$avatar = data['avatar'];
      result$data['avatar'] = l$avatar == null
          ? null
          : Input$String_comparison_exp.fromJson(
              (l$avatar as Map<String, dynamic>));
    }
    if (data.containsKey('city')) {
      final l$city = data['city'];
      result$data['city'] = l$city == null
          ? null
          : Input$String_comparison_exp.fromJson(
              (l$city as Map<String, dynamic>));
    }
    if (data.containsKey('data_cid')) {
      final l$data_cid = data['data_cid'];
      result$data['data_cid'] = l$data_cid == null
          ? null
          : Input$String_comparison_exp.fromJson(
              (l$data_cid as Map<String, dynamic>));
    }
    if (data.containsKey('description')) {
      final l$description = data['description'];
      result$data['description'] = l$description == null
          ? null
          : Input$String_comparison_exp.fromJson(
              (l$description as Map<String, dynamic>));
    }
    if (data.containsKey('geoloc')) {
      final l$geoloc = data['geoloc'];
      result$data['geoloc'] = l$geoloc == null
          ? null
          : Input$point_comparison_exp.fromJson(
              (l$geoloc as Map<String, dynamic>));
    }
    if (data.containsKey('index_request_cid')) {
      final l$index_request_cid = data['index_request_cid'];
      result$data['index_request_cid'] = l$index_request_cid == null
          ? null
          : Input$String_comparison_exp.fromJson(
              (l$index_request_cid as Map<String, dynamic>));
    }
    if (data.containsKey('pubkey')) {
      final l$pubkey = data['pubkey'];
      result$data['pubkey'] = l$pubkey == null
          ? null
          : Input$String_comparison_exp.fromJson(
              (l$pubkey as Map<String, dynamic>));
    }
    if (data.containsKey('socials')) {
      final l$socials = data['socials'];
      result$data['socials'] = l$socials == null
          ? null
          : Input$jsonb_comparison_exp.fromJson(
              (l$socials as Map<String, dynamic>));
    }
    if (data.containsKey('time')) {
      final l$time = data['time'];
      result$data['time'] = l$time == null
          ? null
          : Input$timestamptz_comparison_exp.fromJson(
              (l$time as Map<String, dynamic>));
    }
    if (data.containsKey('title')) {
      final l$title = data['title'];
      result$data['title'] = l$title == null
          ? null
          : Input$String_comparison_exp.fromJson(
              (l$title as Map<String, dynamic>));
    }
    return Input$profiles_bool_exp._(result$data);
  }

  Map<String, dynamic> _$data;

  List<Input$profiles_bool_exp>? get $_and =>
      (_$data['_and'] as List<Input$profiles_bool_exp>?);

  Input$profiles_bool_exp? get $_not =>
      (_$data['_not'] as Input$profiles_bool_exp?);

  List<Input$profiles_bool_exp>? get $_or =>
      (_$data['_or'] as List<Input$profiles_bool_exp>?);

  Input$String_comparison_exp? get avatar =>
      (_$data['avatar'] as Input$String_comparison_exp?);

  Input$String_comparison_exp? get city =>
      (_$data['city'] as Input$String_comparison_exp?);

  Input$String_comparison_exp? get data_cid =>
      (_$data['data_cid'] as Input$String_comparison_exp?);

  Input$String_comparison_exp? get description =>
      (_$data['description'] as Input$String_comparison_exp?);

  Input$point_comparison_exp? get geoloc =>
      (_$data['geoloc'] as Input$point_comparison_exp?);

  Input$String_comparison_exp? get index_request_cid =>
      (_$data['index_request_cid'] as Input$String_comparison_exp?);

  Input$String_comparison_exp? get pubkey =>
      (_$data['pubkey'] as Input$String_comparison_exp?);

  Input$jsonb_comparison_exp? get socials =>
      (_$data['socials'] as Input$jsonb_comparison_exp?);

  Input$timestamptz_comparison_exp? get time =>
      (_$data['time'] as Input$timestamptz_comparison_exp?);

  Input$String_comparison_exp? get title =>
      (_$data['title'] as Input$String_comparison_exp?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    if (_$data.containsKey('_and')) {
      final l$$_and = $_and;
      result$data['_and'] = l$$_and?.map((e) => e.toJson()).toList();
    }
    if (_$data.containsKey('_not')) {
      final l$$_not = $_not;
      result$data['_not'] = l$$_not?.toJson();
    }
    if (_$data.containsKey('_or')) {
      final l$$_or = $_or;
      result$data['_or'] = l$$_or?.map((e) => e.toJson()).toList();
    }
    if (_$data.containsKey('avatar')) {
      final l$avatar = avatar;
      result$data['avatar'] = l$avatar?.toJson();
    }
    if (_$data.containsKey('city')) {
      final l$city = city;
      result$data['city'] = l$city?.toJson();
    }
    if (_$data.containsKey('data_cid')) {
      final l$data_cid = data_cid;
      result$data['data_cid'] = l$data_cid?.toJson();
    }
    if (_$data.containsKey('description')) {
      final l$description = description;
      result$data['description'] = l$description?.toJson();
    }
    if (_$data.containsKey('geoloc')) {
      final l$geoloc = geoloc;
      result$data['geoloc'] = l$geoloc?.toJson();
    }
    if (_$data.containsKey('index_request_cid')) {
      final l$index_request_cid = index_request_cid;
      result$data['index_request_cid'] = l$index_request_cid?.toJson();
    }
    if (_$data.containsKey('pubkey')) {
      final l$pubkey = pubkey;
      result$data['pubkey'] = l$pubkey?.toJson();
    }
    if (_$data.containsKey('socials')) {
      final l$socials = socials;
      result$data['socials'] = l$socials?.toJson();
    }
    if (_$data.containsKey('time')) {
      final l$time = time;
      result$data['time'] = l$time?.toJson();
    }
    if (_$data.containsKey('title')) {
      final l$title = title;
      result$data['title'] = l$title?.toJson();
    }
    return result$data;
  }

  CopyWith$Input$profiles_bool_exp<Input$profiles_bool_exp> get copyWith =>
      CopyWith$Input$profiles_bool_exp(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$profiles_bool_exp) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$$_and = $_and;
    final lOther$$_and = other.$_and;
    if (_$data.containsKey('_and') != other._$data.containsKey('_and')) {
      return false;
    }
    if (l$$_and != null && lOther$$_and != null) {
      if (l$$_and.length != lOther$$_and.length) {
        return false;
      }
      for (int i = 0; i < l$$_and.length; i++) {
        final l$$_and$entry = l$$_and[i];
        final lOther$$_and$entry = lOther$$_and[i];
        if (l$$_and$entry != lOther$$_and$entry) {
          return false;
        }
      }
    } else if (l$$_and != lOther$$_and) {
      return false;
    }
    final l$$_not = $_not;
    final lOther$$_not = other.$_not;
    if (_$data.containsKey('_not') != other._$data.containsKey('_not')) {
      return false;
    }
    if (l$$_not != lOther$$_not) {
      return false;
    }
    final l$$_or = $_or;
    final lOther$$_or = other.$_or;
    if (_$data.containsKey('_or') != other._$data.containsKey('_or')) {
      return false;
    }
    if (l$$_or != null && lOther$$_or != null) {
      if (l$$_or.length != lOther$$_or.length) {
        return false;
      }
      for (int i = 0; i < l$$_or.length; i++) {
        final l$$_or$entry = l$$_or[i];
        final lOther$$_or$entry = lOther$$_or[i];
        if (l$$_or$entry != lOther$$_or$entry) {
          return false;
        }
      }
    } else if (l$$_or != lOther$$_or) {
      return false;
    }
    final l$avatar = avatar;
    final lOther$avatar = other.avatar;
    if (_$data.containsKey('avatar') != other._$data.containsKey('avatar')) {
      return false;
    }
    if (l$avatar != lOther$avatar) {
      return false;
    }
    final l$city = city;
    final lOther$city = other.city;
    if (_$data.containsKey('city') != other._$data.containsKey('city')) {
      return false;
    }
    if (l$city != lOther$city) {
      return false;
    }
    final l$data_cid = data_cid;
    final lOther$data_cid = other.data_cid;
    if (_$data.containsKey('data_cid') !=
        other._$data.containsKey('data_cid')) {
      return false;
    }
    if (l$data_cid != lOther$data_cid) {
      return false;
    }
    final l$description = description;
    final lOther$description = other.description;
    if (_$data.containsKey('description') !=
        other._$data.containsKey('description')) {
      return false;
    }
    if (l$description != lOther$description) {
      return false;
    }
    final l$geoloc = geoloc;
    final lOther$geoloc = other.geoloc;
    if (_$data.containsKey('geoloc') != other._$data.containsKey('geoloc')) {
      return false;
    }
    if (l$geoloc != lOther$geoloc) {
      return false;
    }
    final l$index_request_cid = index_request_cid;
    final lOther$index_request_cid = other.index_request_cid;
    if (_$data.containsKey('index_request_cid') !=
        other._$data.containsKey('index_request_cid')) {
      return false;
    }
    if (l$index_request_cid != lOther$index_request_cid) {
      return false;
    }
    final l$pubkey = pubkey;
    final lOther$pubkey = other.pubkey;
    if (_$data.containsKey('pubkey') != other._$data.containsKey('pubkey')) {
      return false;
    }
    if (l$pubkey != lOther$pubkey) {
      return false;
    }
    final l$socials = socials;
    final lOther$socials = other.socials;
    if (_$data.containsKey('socials') != other._$data.containsKey('socials')) {
      return false;
    }
    if (l$socials != lOther$socials) {
      return false;
    }
    final l$time = time;
    final lOther$time = other.time;
    if (_$data.containsKey('time') != other._$data.containsKey('time')) {
      return false;
    }
    if (l$time != lOther$time) {
      return false;
    }
    final l$title = title;
    final lOther$title = other.title;
    if (_$data.containsKey('title') != other._$data.containsKey('title')) {
      return false;
    }
    if (l$title != lOther$title) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$$_and = $_and;
    final l$$_not = $_not;
    final l$$_or = $_or;
    final l$avatar = avatar;
    final l$city = city;
    final l$data_cid = data_cid;
    final l$description = description;
    final l$geoloc = geoloc;
    final l$index_request_cid = index_request_cid;
    final l$pubkey = pubkey;
    final l$socials = socials;
    final l$time = time;
    final l$title = title;
    return Object.hashAll([
      _$data.containsKey('_and')
          ? l$$_and == null
              ? null
              : Object.hashAll(l$$_and.map((v) => v))
          : const {},
      _$data.containsKey('_not') ? l$$_not : const {},
      _$data.containsKey('_or')
          ? l$$_or == null
              ? null
              : Object.hashAll(l$$_or.map((v) => v))
          : const {},
      _$data.containsKey('avatar') ? l$avatar : const {},
      _$data.containsKey('city') ? l$city : const {},
      _$data.containsKey('data_cid') ? l$data_cid : const {},
      _$data.containsKey('description') ? l$description : const {},
      _$data.containsKey('geoloc') ? l$geoloc : const {},
      _$data.containsKey('index_request_cid') ? l$index_request_cid : const {},
      _$data.containsKey('pubkey') ? l$pubkey : const {},
      _$data.containsKey('socials') ? l$socials : const {},
      _$data.containsKey('time') ? l$time : const {},
      _$data.containsKey('title') ? l$title : const {},
    ]);
  }
}

abstract class CopyWith$Input$profiles_bool_exp<TRes> {
  factory CopyWith$Input$profiles_bool_exp(
    Input$profiles_bool_exp instance,
    TRes Function(Input$profiles_bool_exp) then,
  ) = _CopyWithImpl$Input$profiles_bool_exp;

  factory CopyWith$Input$profiles_bool_exp.stub(TRes res) =
      _CopyWithStubImpl$Input$profiles_bool_exp;

  TRes call({
    List<Input$profiles_bool_exp>? $_and,
    Input$profiles_bool_exp? $_not,
    List<Input$profiles_bool_exp>? $_or,
    Input$String_comparison_exp? avatar,
    Input$String_comparison_exp? city,
    Input$String_comparison_exp? data_cid,
    Input$String_comparison_exp? description,
    Input$point_comparison_exp? geoloc,
    Input$String_comparison_exp? index_request_cid,
    Input$String_comparison_exp? pubkey,
    Input$jsonb_comparison_exp? socials,
    Input$timestamptz_comparison_exp? time,
    Input$String_comparison_exp? title,
  });
  TRes $_and(
      Iterable<Input$profiles_bool_exp>? Function(
              Iterable<
                  CopyWith$Input$profiles_bool_exp<Input$profiles_bool_exp>>?)
          _fn);
  CopyWith$Input$profiles_bool_exp<TRes> get $_not;
  TRes $_or(
      Iterable<Input$profiles_bool_exp>? Function(
              Iterable<
                  CopyWith$Input$profiles_bool_exp<Input$profiles_bool_exp>>?)
          _fn);
  CopyWith$Input$String_comparison_exp<TRes> get avatar;
  CopyWith$Input$String_comparison_exp<TRes> get city;
  CopyWith$Input$String_comparison_exp<TRes> get data_cid;
  CopyWith$Input$String_comparison_exp<TRes> get description;
  CopyWith$Input$point_comparison_exp<TRes> get geoloc;
  CopyWith$Input$String_comparison_exp<TRes> get index_request_cid;
  CopyWith$Input$String_comparison_exp<TRes> get pubkey;
  CopyWith$Input$jsonb_comparison_exp<TRes> get socials;
  CopyWith$Input$timestamptz_comparison_exp<TRes> get time;
  CopyWith$Input$String_comparison_exp<TRes> get title;
}

class _CopyWithImpl$Input$profiles_bool_exp<TRes>
    implements CopyWith$Input$profiles_bool_exp<TRes> {
  _CopyWithImpl$Input$profiles_bool_exp(
    this._instance,
    this._then,
  );

  final Input$profiles_bool_exp _instance;

  final TRes Function(Input$profiles_bool_exp) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? $_and = _undefined,
    Object? $_not = _undefined,
    Object? $_or = _undefined,
    Object? avatar = _undefined,
    Object? city = _undefined,
    Object? data_cid = _undefined,
    Object? description = _undefined,
    Object? geoloc = _undefined,
    Object? index_request_cid = _undefined,
    Object? pubkey = _undefined,
    Object? socials = _undefined,
    Object? time = _undefined,
    Object? title = _undefined,
  }) =>
      _then(Input$profiles_bool_exp._({
        ..._instance._$data,
        if ($_and != _undefined)
          '_and': ($_and as List<Input$profiles_bool_exp>?),
        if ($_not != _undefined) '_not': ($_not as Input$profiles_bool_exp?),
        if ($_or != _undefined) '_or': ($_or as List<Input$profiles_bool_exp>?),
        if (avatar != _undefined)
          'avatar': (avatar as Input$String_comparison_exp?),
        if (city != _undefined) 'city': (city as Input$String_comparison_exp?),
        if (data_cid != _undefined)
          'data_cid': (data_cid as Input$String_comparison_exp?),
        if (description != _undefined)
          'description': (description as Input$String_comparison_exp?),
        if (geoloc != _undefined)
          'geoloc': (geoloc as Input$point_comparison_exp?),
        if (index_request_cid != _undefined)
          'index_request_cid':
              (index_request_cid as Input$String_comparison_exp?),
        if (pubkey != _undefined)
          'pubkey': (pubkey as Input$String_comparison_exp?),
        if (socials != _undefined)
          'socials': (socials as Input$jsonb_comparison_exp?),
        if (time != _undefined)
          'time': (time as Input$timestamptz_comparison_exp?),
        if (title != _undefined)
          'title': (title as Input$String_comparison_exp?),
      }));

  TRes $_and(
          Iterable<Input$profiles_bool_exp>? Function(
                  Iterable<
                      CopyWith$Input$profiles_bool_exp<
                          Input$profiles_bool_exp>>?)
              _fn) =>
      call(
          $_and:
              _fn(_instance.$_and?.map((e) => CopyWith$Input$profiles_bool_exp(
                    e,
                    (i) => i,
                  )))?.toList());

  CopyWith$Input$profiles_bool_exp<TRes> get $_not {
    final local$$_not = _instance.$_not;
    return local$$_not == null
        ? CopyWith$Input$profiles_bool_exp.stub(_then(_instance))
        : CopyWith$Input$profiles_bool_exp(local$$_not, (e) => call($_not: e));
  }

  TRes $_or(
          Iterable<Input$profiles_bool_exp>? Function(
                  Iterable<
                      CopyWith$Input$profiles_bool_exp<
                          Input$profiles_bool_exp>>?)
              _fn) =>
      call(
          $_or: _fn(_instance.$_or?.map((e) => CopyWith$Input$profiles_bool_exp(
                e,
                (i) => i,
              )))?.toList());

  CopyWith$Input$String_comparison_exp<TRes> get avatar {
    final local$avatar = _instance.avatar;
    return local$avatar == null
        ? CopyWith$Input$String_comparison_exp.stub(_then(_instance))
        : CopyWith$Input$String_comparison_exp(
            local$avatar, (e) => call(avatar: e));
  }

  CopyWith$Input$String_comparison_exp<TRes> get city {
    final local$city = _instance.city;
    return local$city == null
        ? CopyWith$Input$String_comparison_exp.stub(_then(_instance))
        : CopyWith$Input$String_comparison_exp(
            local$city, (e) => call(city: e));
  }

  CopyWith$Input$String_comparison_exp<TRes> get data_cid {
    final local$data_cid = _instance.data_cid;
    return local$data_cid == null
        ? CopyWith$Input$String_comparison_exp.stub(_then(_instance))
        : CopyWith$Input$String_comparison_exp(
            local$data_cid, (e) => call(data_cid: e));
  }

  CopyWith$Input$String_comparison_exp<TRes> get description {
    final local$description = _instance.description;
    return local$description == null
        ? CopyWith$Input$String_comparison_exp.stub(_then(_instance))
        : CopyWith$Input$String_comparison_exp(
            local$description, (e) => call(description: e));
  }

  CopyWith$Input$point_comparison_exp<TRes> get geoloc {
    final local$geoloc = _instance.geoloc;
    return local$geoloc == null
        ? CopyWith$Input$point_comparison_exp.stub(_then(_instance))
        : CopyWith$Input$point_comparison_exp(
            local$geoloc, (e) => call(geoloc: e));
  }

  CopyWith$Input$String_comparison_exp<TRes> get index_request_cid {
    final local$index_request_cid = _instance.index_request_cid;
    return local$index_request_cid == null
        ? CopyWith$Input$String_comparison_exp.stub(_then(_instance))
        : CopyWith$Input$String_comparison_exp(
            local$index_request_cid, (e) => call(index_request_cid: e));
  }

  CopyWith$Input$String_comparison_exp<TRes> get pubkey {
    final local$pubkey = _instance.pubkey;
    return local$pubkey == null
        ? CopyWith$Input$String_comparison_exp.stub(_then(_instance))
        : CopyWith$Input$String_comparison_exp(
            local$pubkey, (e) => call(pubkey: e));
  }

  CopyWith$Input$jsonb_comparison_exp<TRes> get socials {
    final local$socials = _instance.socials;
    return local$socials == null
        ? CopyWith$Input$jsonb_comparison_exp.stub(_then(_instance))
        : CopyWith$Input$jsonb_comparison_exp(
            local$socials, (e) => call(socials: e));
  }

  CopyWith$Input$timestamptz_comparison_exp<TRes> get time {
    final local$time = _instance.time;
    return local$time == null
        ? CopyWith$Input$timestamptz_comparison_exp.stub(_then(_instance))
        : CopyWith$Input$timestamptz_comparison_exp(
            local$time, (e) => call(time: e));
  }

  CopyWith$Input$String_comparison_exp<TRes> get title {
    final local$title = _instance.title;
    return local$title == null
        ? CopyWith$Input$String_comparison_exp.stub(_then(_instance))
        : CopyWith$Input$String_comparison_exp(
            local$title, (e) => call(title: e));
  }
}

class _CopyWithStubImpl$Input$profiles_bool_exp<TRes>
    implements CopyWith$Input$profiles_bool_exp<TRes> {
  _CopyWithStubImpl$Input$profiles_bool_exp(this._res);

  TRes _res;

  call({
    List<Input$profiles_bool_exp>? $_and,
    Input$profiles_bool_exp? $_not,
    List<Input$profiles_bool_exp>? $_or,
    Input$String_comparison_exp? avatar,
    Input$String_comparison_exp? city,
    Input$String_comparison_exp? data_cid,
    Input$String_comparison_exp? description,
    Input$point_comparison_exp? geoloc,
    Input$String_comparison_exp? index_request_cid,
    Input$String_comparison_exp? pubkey,
    Input$jsonb_comparison_exp? socials,
    Input$timestamptz_comparison_exp? time,
    Input$String_comparison_exp? title,
  }) =>
      _res;

  $_and(_fn) => _res;

  CopyWith$Input$profiles_bool_exp<TRes> get $_not =>
      CopyWith$Input$profiles_bool_exp.stub(_res);

  $_or(_fn) => _res;

  CopyWith$Input$String_comparison_exp<TRes> get avatar =>
      CopyWith$Input$String_comparison_exp.stub(_res);

  CopyWith$Input$String_comparison_exp<TRes> get city =>
      CopyWith$Input$String_comparison_exp.stub(_res);

  CopyWith$Input$String_comparison_exp<TRes> get data_cid =>
      CopyWith$Input$String_comparison_exp.stub(_res);

  CopyWith$Input$String_comparison_exp<TRes> get description =>
      CopyWith$Input$String_comparison_exp.stub(_res);

  CopyWith$Input$point_comparison_exp<TRes> get geoloc =>
      CopyWith$Input$point_comparison_exp.stub(_res);

  CopyWith$Input$String_comparison_exp<TRes> get index_request_cid =>
      CopyWith$Input$String_comparison_exp.stub(_res);

  CopyWith$Input$String_comparison_exp<TRes> get pubkey =>
      CopyWith$Input$String_comparison_exp.stub(_res);

  CopyWith$Input$jsonb_comparison_exp<TRes> get socials =>
      CopyWith$Input$jsonb_comparison_exp.stub(_res);

  CopyWith$Input$timestamptz_comparison_exp<TRes> get time =>
      CopyWith$Input$timestamptz_comparison_exp.stub(_res);

  CopyWith$Input$String_comparison_exp<TRes> get title =>
      CopyWith$Input$String_comparison_exp.stub(_res);
}

class Input$profiles_order_by {
  factory Input$profiles_order_by({
    Enum$order_by? avatar,
    Enum$order_by? city,
    Enum$order_by? data_cid,
    Enum$order_by? description,
    Enum$order_by? geoloc,
    Enum$order_by? index_request_cid,
    Enum$order_by? pubkey,
    Enum$order_by? socials,
    Enum$order_by? time,
    Enum$order_by? title,
  }) =>
      Input$profiles_order_by._({
        if (avatar != null) r'avatar': avatar,
        if (city != null) r'city': city,
        if (data_cid != null) r'data_cid': data_cid,
        if (description != null) r'description': description,
        if (geoloc != null) r'geoloc': geoloc,
        if (index_request_cid != null) r'index_request_cid': index_request_cid,
        if (pubkey != null) r'pubkey': pubkey,
        if (socials != null) r'socials': socials,
        if (time != null) r'time': time,
        if (title != null) r'title': title,
      });

  Input$profiles_order_by._(this._$data);

  factory Input$profiles_order_by.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    if (data.containsKey('avatar')) {
      final l$avatar = data['avatar'];
      result$data['avatar'] = l$avatar == null
          ? null
          : fromJson$Enum$order_by((l$avatar as String));
    }
    if (data.containsKey('city')) {
      final l$city = data['city'];
      result$data['city'] =
          l$city == null ? null : fromJson$Enum$order_by((l$city as String));
    }
    if (data.containsKey('data_cid')) {
      final l$data_cid = data['data_cid'];
      result$data['data_cid'] = l$data_cid == null
          ? null
          : fromJson$Enum$order_by((l$data_cid as String));
    }
    if (data.containsKey('description')) {
      final l$description = data['description'];
      result$data['description'] = l$description == null
          ? null
          : fromJson$Enum$order_by((l$description as String));
    }
    if (data.containsKey('geoloc')) {
      final l$geoloc = data['geoloc'];
      result$data['geoloc'] = l$geoloc == null
          ? null
          : fromJson$Enum$order_by((l$geoloc as String));
    }
    if (data.containsKey('index_request_cid')) {
      final l$index_request_cid = data['index_request_cid'];
      result$data['index_request_cid'] = l$index_request_cid == null
          ? null
          : fromJson$Enum$order_by((l$index_request_cid as String));
    }
    if (data.containsKey('pubkey')) {
      final l$pubkey = data['pubkey'];
      result$data['pubkey'] = l$pubkey == null
          ? null
          : fromJson$Enum$order_by((l$pubkey as String));
    }
    if (data.containsKey('socials')) {
      final l$socials = data['socials'];
      result$data['socials'] = l$socials == null
          ? null
          : fromJson$Enum$order_by((l$socials as String));
    }
    if (data.containsKey('time')) {
      final l$time = data['time'];
      result$data['time'] =
          l$time == null ? null : fromJson$Enum$order_by((l$time as String));
    }
    if (data.containsKey('title')) {
      final l$title = data['title'];
      result$data['title'] =
          l$title == null ? null : fromJson$Enum$order_by((l$title as String));
    }
    return Input$profiles_order_by._(result$data);
  }

  Map<String, dynamic> _$data;

  Enum$order_by? get avatar => (_$data['avatar'] as Enum$order_by?);

  Enum$order_by? get city => (_$data['city'] as Enum$order_by?);

  Enum$order_by? get data_cid => (_$data['data_cid'] as Enum$order_by?);

  Enum$order_by? get description => (_$data['description'] as Enum$order_by?);

  Enum$order_by? get geoloc => (_$data['geoloc'] as Enum$order_by?);

  Enum$order_by? get index_request_cid =>
      (_$data['index_request_cid'] as Enum$order_by?);

  Enum$order_by? get pubkey => (_$data['pubkey'] as Enum$order_by?);

  Enum$order_by? get socials => (_$data['socials'] as Enum$order_by?);

  Enum$order_by? get time => (_$data['time'] as Enum$order_by?);

  Enum$order_by? get title => (_$data['title'] as Enum$order_by?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    if (_$data.containsKey('avatar')) {
      final l$avatar = avatar;
      result$data['avatar'] =
          l$avatar == null ? null : toJson$Enum$order_by(l$avatar);
    }
    if (_$data.containsKey('city')) {
      final l$city = city;
      result$data['city'] =
          l$city == null ? null : toJson$Enum$order_by(l$city);
    }
    if (_$data.containsKey('data_cid')) {
      final l$data_cid = data_cid;
      result$data['data_cid'] =
          l$data_cid == null ? null : toJson$Enum$order_by(l$data_cid);
    }
    if (_$data.containsKey('description')) {
      final l$description = description;
      result$data['description'] =
          l$description == null ? null : toJson$Enum$order_by(l$description);
    }
    if (_$data.containsKey('geoloc')) {
      final l$geoloc = geoloc;
      result$data['geoloc'] =
          l$geoloc == null ? null : toJson$Enum$order_by(l$geoloc);
    }
    if (_$data.containsKey('index_request_cid')) {
      final l$index_request_cid = index_request_cid;
      result$data['index_request_cid'] = l$index_request_cid == null
          ? null
          : toJson$Enum$order_by(l$index_request_cid);
    }
    if (_$data.containsKey('pubkey')) {
      final l$pubkey = pubkey;
      result$data['pubkey'] =
          l$pubkey == null ? null : toJson$Enum$order_by(l$pubkey);
    }
    if (_$data.containsKey('socials')) {
      final l$socials = socials;
      result$data['socials'] =
          l$socials == null ? null : toJson$Enum$order_by(l$socials);
    }
    if (_$data.containsKey('time')) {
      final l$time = time;
      result$data['time'] =
          l$time == null ? null : toJson$Enum$order_by(l$time);
    }
    if (_$data.containsKey('title')) {
      final l$title = title;
      result$data['title'] =
          l$title == null ? null : toJson$Enum$order_by(l$title);
    }
    return result$data;
  }

  CopyWith$Input$profiles_order_by<Input$profiles_order_by> get copyWith =>
      CopyWith$Input$profiles_order_by(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$profiles_order_by) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$avatar = avatar;
    final lOther$avatar = other.avatar;
    if (_$data.containsKey('avatar') != other._$data.containsKey('avatar')) {
      return false;
    }
    if (l$avatar != lOther$avatar) {
      return false;
    }
    final l$city = city;
    final lOther$city = other.city;
    if (_$data.containsKey('city') != other._$data.containsKey('city')) {
      return false;
    }
    if (l$city != lOther$city) {
      return false;
    }
    final l$data_cid = data_cid;
    final lOther$data_cid = other.data_cid;
    if (_$data.containsKey('data_cid') !=
        other._$data.containsKey('data_cid')) {
      return false;
    }
    if (l$data_cid != lOther$data_cid) {
      return false;
    }
    final l$description = description;
    final lOther$description = other.description;
    if (_$data.containsKey('description') !=
        other._$data.containsKey('description')) {
      return false;
    }
    if (l$description != lOther$description) {
      return false;
    }
    final l$geoloc = geoloc;
    final lOther$geoloc = other.geoloc;
    if (_$data.containsKey('geoloc') != other._$data.containsKey('geoloc')) {
      return false;
    }
    if (l$geoloc != lOther$geoloc) {
      return false;
    }
    final l$index_request_cid = index_request_cid;
    final lOther$index_request_cid = other.index_request_cid;
    if (_$data.containsKey('index_request_cid') !=
        other._$data.containsKey('index_request_cid')) {
      return false;
    }
    if (l$index_request_cid != lOther$index_request_cid) {
      return false;
    }
    final l$pubkey = pubkey;
    final lOther$pubkey = other.pubkey;
    if (_$data.containsKey('pubkey') != other._$data.containsKey('pubkey')) {
      return false;
    }
    if (l$pubkey != lOther$pubkey) {
      return false;
    }
    final l$socials = socials;
    final lOther$socials = other.socials;
    if (_$data.containsKey('socials') != other._$data.containsKey('socials')) {
      return false;
    }
    if (l$socials != lOther$socials) {
      return false;
    }
    final l$time = time;
    final lOther$time = other.time;
    if (_$data.containsKey('time') != other._$data.containsKey('time')) {
      return false;
    }
    if (l$time != lOther$time) {
      return false;
    }
    final l$title = title;
    final lOther$title = other.title;
    if (_$data.containsKey('title') != other._$data.containsKey('title')) {
      return false;
    }
    if (l$title != lOther$title) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$avatar = avatar;
    final l$city = city;
    final l$data_cid = data_cid;
    final l$description = description;
    final l$geoloc = geoloc;
    final l$index_request_cid = index_request_cid;
    final l$pubkey = pubkey;
    final l$socials = socials;
    final l$time = time;
    final l$title = title;
    return Object.hashAll([
      _$data.containsKey('avatar') ? l$avatar : const {},
      _$data.containsKey('city') ? l$city : const {},
      _$data.containsKey('data_cid') ? l$data_cid : const {},
      _$data.containsKey('description') ? l$description : const {},
      _$data.containsKey('geoloc') ? l$geoloc : const {},
      _$data.containsKey('index_request_cid') ? l$index_request_cid : const {},
      _$data.containsKey('pubkey') ? l$pubkey : const {},
      _$data.containsKey('socials') ? l$socials : const {},
      _$data.containsKey('time') ? l$time : const {},
      _$data.containsKey('title') ? l$title : const {},
    ]);
  }
}

abstract class CopyWith$Input$profiles_order_by<TRes> {
  factory CopyWith$Input$profiles_order_by(
    Input$profiles_order_by instance,
    TRes Function(Input$profiles_order_by) then,
  ) = _CopyWithImpl$Input$profiles_order_by;

  factory CopyWith$Input$profiles_order_by.stub(TRes res) =
      _CopyWithStubImpl$Input$profiles_order_by;

  TRes call({
    Enum$order_by? avatar,
    Enum$order_by? city,
    Enum$order_by? data_cid,
    Enum$order_by? description,
    Enum$order_by? geoloc,
    Enum$order_by? index_request_cid,
    Enum$order_by? pubkey,
    Enum$order_by? socials,
    Enum$order_by? time,
    Enum$order_by? title,
  });
}

class _CopyWithImpl$Input$profiles_order_by<TRes>
    implements CopyWith$Input$profiles_order_by<TRes> {
  _CopyWithImpl$Input$profiles_order_by(
    this._instance,
    this._then,
  );

  final Input$profiles_order_by _instance;

  final TRes Function(Input$profiles_order_by) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? avatar = _undefined,
    Object? city = _undefined,
    Object? data_cid = _undefined,
    Object? description = _undefined,
    Object? geoloc = _undefined,
    Object? index_request_cid = _undefined,
    Object? pubkey = _undefined,
    Object? socials = _undefined,
    Object? time = _undefined,
    Object? title = _undefined,
  }) =>
      _then(Input$profiles_order_by._({
        ..._instance._$data,
        if (avatar != _undefined) 'avatar': (avatar as Enum$order_by?),
        if (city != _undefined) 'city': (city as Enum$order_by?),
        if (data_cid != _undefined) 'data_cid': (data_cid as Enum$order_by?),
        if (description != _undefined)
          'description': (description as Enum$order_by?),
        if (geoloc != _undefined) 'geoloc': (geoloc as Enum$order_by?),
        if (index_request_cid != _undefined)
          'index_request_cid': (index_request_cid as Enum$order_by?),
        if (pubkey != _undefined) 'pubkey': (pubkey as Enum$order_by?),
        if (socials != _undefined) 'socials': (socials as Enum$order_by?),
        if (time != _undefined) 'time': (time as Enum$order_by?),
        if (title != _undefined) 'title': (title as Enum$order_by?),
      }));
}

class _CopyWithStubImpl$Input$profiles_order_by<TRes>
    implements CopyWith$Input$profiles_order_by<TRes> {
  _CopyWithStubImpl$Input$profiles_order_by(this._res);

  TRes _res;

  call({
    Enum$order_by? avatar,
    Enum$order_by? city,
    Enum$order_by? data_cid,
    Enum$order_by? description,
    Enum$order_by? geoloc,
    Enum$order_by? index_request_cid,
    Enum$order_by? pubkey,
    Enum$order_by? socials,
    Enum$order_by? time,
    Enum$order_by? title,
  }) =>
      _res;
}

class Input$profiles_stream_cursor_input {
  factory Input$profiles_stream_cursor_input({
    required Input$profiles_stream_cursor_value_input initial_value,
    Enum$cursor_ordering? ordering,
  }) =>
      Input$profiles_stream_cursor_input._({
        r'initial_value': initial_value,
        if (ordering != null) r'ordering': ordering,
      });

  Input$profiles_stream_cursor_input._(this._$data);

  factory Input$profiles_stream_cursor_input.fromJson(
      Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    final l$initial_value = data['initial_value'];
    result$data['initial_value'] =
        Input$profiles_stream_cursor_value_input.fromJson(
            (l$initial_value as Map<String, dynamic>));
    if (data.containsKey('ordering')) {
      final l$ordering = data['ordering'];
      result$data['ordering'] = l$ordering == null
          ? null
          : fromJson$Enum$cursor_ordering((l$ordering as String));
    }
    return Input$profiles_stream_cursor_input._(result$data);
  }

  Map<String, dynamic> _$data;

  Input$profiles_stream_cursor_value_input get initial_value =>
      (_$data['initial_value'] as Input$profiles_stream_cursor_value_input);

  Enum$cursor_ordering? get ordering =>
      (_$data['ordering'] as Enum$cursor_ordering?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    final l$initial_value = initial_value;
    result$data['initial_value'] = l$initial_value.toJson();
    if (_$data.containsKey('ordering')) {
      final l$ordering = ordering;
      result$data['ordering'] =
          l$ordering == null ? null : toJson$Enum$cursor_ordering(l$ordering);
    }
    return result$data;
  }

  CopyWith$Input$profiles_stream_cursor_input<
          Input$profiles_stream_cursor_input>
      get copyWith => CopyWith$Input$profiles_stream_cursor_input(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$profiles_stream_cursor_input) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$initial_value = initial_value;
    final lOther$initial_value = other.initial_value;
    if (l$initial_value != lOther$initial_value) {
      return false;
    }
    final l$ordering = ordering;
    final lOther$ordering = other.ordering;
    if (_$data.containsKey('ordering') !=
        other._$data.containsKey('ordering')) {
      return false;
    }
    if (l$ordering != lOther$ordering) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$initial_value = initial_value;
    final l$ordering = ordering;
    return Object.hashAll([
      l$initial_value,
      _$data.containsKey('ordering') ? l$ordering : const {},
    ]);
  }
}

abstract class CopyWith$Input$profiles_stream_cursor_input<TRes> {
  factory CopyWith$Input$profiles_stream_cursor_input(
    Input$profiles_stream_cursor_input instance,
    TRes Function(Input$profiles_stream_cursor_input) then,
  ) = _CopyWithImpl$Input$profiles_stream_cursor_input;

  factory CopyWith$Input$profiles_stream_cursor_input.stub(TRes res) =
      _CopyWithStubImpl$Input$profiles_stream_cursor_input;

  TRes call({
    Input$profiles_stream_cursor_value_input? initial_value,
    Enum$cursor_ordering? ordering,
  });
  CopyWith$Input$profiles_stream_cursor_value_input<TRes> get initial_value;
}

class _CopyWithImpl$Input$profiles_stream_cursor_input<TRes>
    implements CopyWith$Input$profiles_stream_cursor_input<TRes> {
  _CopyWithImpl$Input$profiles_stream_cursor_input(
    this._instance,
    this._then,
  );

  final Input$profiles_stream_cursor_input _instance;

  final TRes Function(Input$profiles_stream_cursor_input) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? initial_value = _undefined,
    Object? ordering = _undefined,
  }) =>
      _then(Input$profiles_stream_cursor_input._({
        ..._instance._$data,
        if (initial_value != _undefined && initial_value != null)
          'initial_value':
              (initial_value as Input$profiles_stream_cursor_value_input),
        if (ordering != _undefined)
          'ordering': (ordering as Enum$cursor_ordering?),
      }));

  CopyWith$Input$profiles_stream_cursor_value_input<TRes> get initial_value {
    final local$initial_value = _instance.initial_value;
    return CopyWith$Input$profiles_stream_cursor_value_input(
        local$initial_value, (e) => call(initial_value: e));
  }
}

class _CopyWithStubImpl$Input$profiles_stream_cursor_input<TRes>
    implements CopyWith$Input$profiles_stream_cursor_input<TRes> {
  _CopyWithStubImpl$Input$profiles_stream_cursor_input(this._res);

  TRes _res;

  call({
    Input$profiles_stream_cursor_value_input? initial_value,
    Enum$cursor_ordering? ordering,
  }) =>
      _res;

  CopyWith$Input$profiles_stream_cursor_value_input<TRes> get initial_value =>
      CopyWith$Input$profiles_stream_cursor_value_input.stub(_res);
}

class Input$profiles_stream_cursor_value_input {
  factory Input$profiles_stream_cursor_value_input({
    String? avatar,
    String? city,
    String? data_cid,
    String? description,
    String? geoloc,
    String? index_request_cid,
    String? pubkey,
    Map<String, dynamic>? socials,
    DateTime? time,
    String? title,
  }) =>
      Input$profiles_stream_cursor_value_input._({
        if (avatar != null) r'avatar': avatar,
        if (city != null) r'city': city,
        if (data_cid != null) r'data_cid': data_cid,
        if (description != null) r'description': description,
        if (geoloc != null) r'geoloc': geoloc,
        if (index_request_cid != null) r'index_request_cid': index_request_cid,
        if (pubkey != null) r'pubkey': pubkey,
        if (socials != null) r'socials': socials,
        if (time != null) r'time': time,
        if (title != null) r'title': title,
      });

  Input$profiles_stream_cursor_value_input._(this._$data);

  factory Input$profiles_stream_cursor_value_input.fromJson(
      Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    if (data.containsKey('avatar')) {
      final l$avatar = data['avatar'];
      result$data['avatar'] = (l$avatar as String?);
    }
    if (data.containsKey('city')) {
      final l$city = data['city'];
      result$data['city'] = (l$city as String?);
    }
    if (data.containsKey('data_cid')) {
      final l$data_cid = data['data_cid'];
      result$data['data_cid'] = (l$data_cid as String?);
    }
    if (data.containsKey('description')) {
      final l$description = data['description'];
      result$data['description'] = (l$description as String?);
    }
    if (data.containsKey('geoloc')) {
      final l$geoloc = data['geoloc'];
      result$data['geoloc'] = (l$geoloc as String?);
    }
    if (data.containsKey('index_request_cid')) {
      final l$index_request_cid = data['index_request_cid'];
      result$data['index_request_cid'] = (l$index_request_cid as String?);
    }
    if (data.containsKey('pubkey')) {
      final l$pubkey = data['pubkey'];
      result$data['pubkey'] = (l$pubkey as String?);
    }
    if (data.containsKey('socials')) {
      final l$socials = data['socials'];
      result$data['socials'] = (l$socials as Map<String, dynamic>?);
    }
    if (data.containsKey('time')) {
      final l$time = data['time'];
      result$data['time'] =
          l$time == null ? null : DateTime.parse((l$time as String));
    }
    if (data.containsKey('title')) {
      final l$title = data['title'];
      result$data['title'] = (l$title as String?);
    }
    return Input$profiles_stream_cursor_value_input._(result$data);
  }

  Map<String, dynamic> _$data;

  String? get avatar => (_$data['avatar'] as String?);

  String? get city => (_$data['city'] as String?);

  String? get data_cid => (_$data['data_cid'] as String?);

  String? get description => (_$data['description'] as String?);

  String? get geoloc => (_$data['geoloc'] as String?);

  String? get index_request_cid => (_$data['index_request_cid'] as String?);

  String? get pubkey => (_$data['pubkey'] as String?);

  Map<String, dynamic>? get socials =>
      (_$data['socials'] as Map<String, dynamic>?);

  DateTime? get time => (_$data['time'] as DateTime?);

  String? get title => (_$data['title'] as String?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    if (_$data.containsKey('avatar')) {
      final l$avatar = avatar;
      result$data['avatar'] = l$avatar;
    }
    if (_$data.containsKey('city')) {
      final l$city = city;
      result$data['city'] = l$city;
    }
    if (_$data.containsKey('data_cid')) {
      final l$data_cid = data_cid;
      result$data['data_cid'] = l$data_cid;
    }
    if (_$data.containsKey('description')) {
      final l$description = description;
      result$data['description'] = l$description;
    }
    if (_$data.containsKey('geoloc')) {
      final l$geoloc = geoloc;
      result$data['geoloc'] = l$geoloc;
    }
    if (_$data.containsKey('index_request_cid')) {
      final l$index_request_cid = index_request_cid;
      result$data['index_request_cid'] = l$index_request_cid;
    }
    if (_$data.containsKey('pubkey')) {
      final l$pubkey = pubkey;
      result$data['pubkey'] = l$pubkey;
    }
    if (_$data.containsKey('socials')) {
      final l$socials = socials;
      result$data['socials'] = l$socials;
    }
    if (_$data.containsKey('time')) {
      final l$time = time;
      result$data['time'] = l$time?.toIso8601String();
    }
    if (_$data.containsKey('title')) {
      final l$title = title;
      result$data['title'] = l$title;
    }
    return result$data;
  }

  CopyWith$Input$profiles_stream_cursor_value_input<
          Input$profiles_stream_cursor_value_input>
      get copyWith => CopyWith$Input$profiles_stream_cursor_value_input(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$profiles_stream_cursor_value_input) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$avatar = avatar;
    final lOther$avatar = other.avatar;
    if (_$data.containsKey('avatar') != other._$data.containsKey('avatar')) {
      return false;
    }
    if (l$avatar != lOther$avatar) {
      return false;
    }
    final l$city = city;
    final lOther$city = other.city;
    if (_$data.containsKey('city') != other._$data.containsKey('city')) {
      return false;
    }
    if (l$city != lOther$city) {
      return false;
    }
    final l$data_cid = data_cid;
    final lOther$data_cid = other.data_cid;
    if (_$data.containsKey('data_cid') !=
        other._$data.containsKey('data_cid')) {
      return false;
    }
    if (l$data_cid != lOther$data_cid) {
      return false;
    }
    final l$description = description;
    final lOther$description = other.description;
    if (_$data.containsKey('description') !=
        other._$data.containsKey('description')) {
      return false;
    }
    if (l$description != lOther$description) {
      return false;
    }
    final l$geoloc = geoloc;
    final lOther$geoloc = other.geoloc;
    if (_$data.containsKey('geoloc') != other._$data.containsKey('geoloc')) {
      return false;
    }
    if (l$geoloc != lOther$geoloc) {
      return false;
    }
    final l$index_request_cid = index_request_cid;
    final lOther$index_request_cid = other.index_request_cid;
    if (_$data.containsKey('index_request_cid') !=
        other._$data.containsKey('index_request_cid')) {
      return false;
    }
    if (l$index_request_cid != lOther$index_request_cid) {
      return false;
    }
    final l$pubkey = pubkey;
    final lOther$pubkey = other.pubkey;
    if (_$data.containsKey('pubkey') != other._$data.containsKey('pubkey')) {
      return false;
    }
    if (l$pubkey != lOther$pubkey) {
      return false;
    }
    final l$socials = socials;
    final lOther$socials = other.socials;
    if (_$data.containsKey('socials') != other._$data.containsKey('socials')) {
      return false;
    }
    if (l$socials != lOther$socials) {
      return false;
    }
    final l$time = time;
    final lOther$time = other.time;
    if (_$data.containsKey('time') != other._$data.containsKey('time')) {
      return false;
    }
    if (l$time != lOther$time) {
      return false;
    }
    final l$title = title;
    final lOther$title = other.title;
    if (_$data.containsKey('title') != other._$data.containsKey('title')) {
      return false;
    }
    if (l$title != lOther$title) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$avatar = avatar;
    final l$city = city;
    final l$data_cid = data_cid;
    final l$description = description;
    final l$geoloc = geoloc;
    final l$index_request_cid = index_request_cid;
    final l$pubkey = pubkey;
    final l$socials = socials;
    final l$time = time;
    final l$title = title;
    return Object.hashAll([
      _$data.containsKey('avatar') ? l$avatar : const {},
      _$data.containsKey('city') ? l$city : const {},
      _$data.containsKey('data_cid') ? l$data_cid : const {},
      _$data.containsKey('description') ? l$description : const {},
      _$data.containsKey('geoloc') ? l$geoloc : const {},
      _$data.containsKey('index_request_cid') ? l$index_request_cid : const {},
      _$data.containsKey('pubkey') ? l$pubkey : const {},
      _$data.containsKey('socials') ? l$socials : const {},
      _$data.containsKey('time') ? l$time : const {},
      _$data.containsKey('title') ? l$title : const {},
    ]);
  }
}

abstract class CopyWith$Input$profiles_stream_cursor_value_input<TRes> {
  factory CopyWith$Input$profiles_stream_cursor_value_input(
    Input$profiles_stream_cursor_value_input instance,
    TRes Function(Input$profiles_stream_cursor_value_input) then,
  ) = _CopyWithImpl$Input$profiles_stream_cursor_value_input;

  factory CopyWith$Input$profiles_stream_cursor_value_input.stub(TRes res) =
      _CopyWithStubImpl$Input$profiles_stream_cursor_value_input;

  TRes call({
    String? avatar,
    String? city,
    String? data_cid,
    String? description,
    String? geoloc,
    String? index_request_cid,
    String? pubkey,
    Map<String, dynamic>? socials,
    DateTime? time,
    String? title,
  });
}

class _CopyWithImpl$Input$profiles_stream_cursor_value_input<TRes>
    implements CopyWith$Input$profiles_stream_cursor_value_input<TRes> {
  _CopyWithImpl$Input$profiles_stream_cursor_value_input(
    this._instance,
    this._then,
  );

  final Input$profiles_stream_cursor_value_input _instance;

  final TRes Function(Input$profiles_stream_cursor_value_input) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? avatar = _undefined,
    Object? city = _undefined,
    Object? data_cid = _undefined,
    Object? description = _undefined,
    Object? geoloc = _undefined,
    Object? index_request_cid = _undefined,
    Object? pubkey = _undefined,
    Object? socials = _undefined,
    Object? time = _undefined,
    Object? title = _undefined,
  }) =>
      _then(Input$profiles_stream_cursor_value_input._({
        ..._instance._$data,
        if (avatar != _undefined) 'avatar': (avatar as String?),
        if (city != _undefined) 'city': (city as String?),
        if (data_cid != _undefined) 'data_cid': (data_cid as String?),
        if (description != _undefined) 'description': (description as String?),
        if (geoloc != _undefined) 'geoloc': (geoloc as String?),
        if (index_request_cid != _undefined)
          'index_request_cid': (index_request_cid as String?),
        if (pubkey != _undefined) 'pubkey': (pubkey as String?),
        if (socials != _undefined)
          'socials': (socials as Map<String, dynamic>?),
        if (time != _undefined) 'time': (time as DateTime?),
        if (title != _undefined) 'title': (title as String?),
      }));
}

class _CopyWithStubImpl$Input$profiles_stream_cursor_value_input<TRes>
    implements CopyWith$Input$profiles_stream_cursor_value_input<TRes> {
  _CopyWithStubImpl$Input$profiles_stream_cursor_value_input(this._res);

  TRes _res;

  call({
    String? avatar,
    String? city,
    String? data_cid,
    String? description,
    String? geoloc,
    String? index_request_cid,
    String? pubkey,
    Map<String, dynamic>? socials,
    DateTime? time,
    String? title,
  }) =>
      _res;
}

class Input$SocialInput {
  factory Input$SocialInput({
    String? type,
    required String url,
  }) =>
      Input$SocialInput._({
        if (type != null) r'type': type,
        r'url': url,
      });

  Input$SocialInput._(this._$data);

  factory Input$SocialInput.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    if (data.containsKey('type')) {
      final l$type = data['type'];
      result$data['type'] = (l$type as String?);
    }
    final l$url = data['url'];
    result$data['url'] = (l$url as String);
    return Input$SocialInput._(result$data);
  }

  Map<String, dynamic> _$data;

  String? get type => (_$data['type'] as String?);

  String get url => (_$data['url'] as String);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    if (_$data.containsKey('type')) {
      final l$type = type;
      result$data['type'] = l$type;
    }
    final l$url = url;
    result$data['url'] = l$url;
    return result$data;
  }

  CopyWith$Input$SocialInput<Input$SocialInput> get copyWith =>
      CopyWith$Input$SocialInput(
        this,
        (i) => i,
      );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$SocialInput) || runtimeType != other.runtimeType) {
      return false;
    }
    final l$type = type;
    final lOther$type = other.type;
    if (_$data.containsKey('type') != other._$data.containsKey('type')) {
      return false;
    }
    if (l$type != lOther$type) {
      return false;
    }
    final l$url = url;
    final lOther$url = other.url;
    if (l$url != lOther$url) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$type = type;
    final l$url = url;
    return Object.hashAll([
      _$data.containsKey('type') ? l$type : const {},
      l$url,
    ]);
  }
}

abstract class CopyWith$Input$SocialInput<TRes> {
  factory CopyWith$Input$SocialInput(
    Input$SocialInput instance,
    TRes Function(Input$SocialInput) then,
  ) = _CopyWithImpl$Input$SocialInput;

  factory CopyWith$Input$SocialInput.stub(TRes res) =
      _CopyWithStubImpl$Input$SocialInput;

  TRes call({
    String? type,
    String? url,
  });
}

class _CopyWithImpl$Input$SocialInput<TRes>
    implements CopyWith$Input$SocialInput<TRes> {
  _CopyWithImpl$Input$SocialInput(
    this._instance,
    this._then,
  );

  final Input$SocialInput _instance;

  final TRes Function(Input$SocialInput) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? type = _undefined,
    Object? url = _undefined,
  }) =>
      _then(Input$SocialInput._({
        ..._instance._$data,
        if (type != _undefined) 'type': (type as String?),
        if (url != _undefined && url != null) 'url': (url as String),
      }));
}

class _CopyWithStubImpl$Input$SocialInput<TRes>
    implements CopyWith$Input$SocialInput<TRes> {
  _CopyWithStubImpl$Input$SocialInput(this._res);

  TRes _res;

  call({
    String? type,
    String? url,
  }) =>
      _res;
}

class Input$String_comparison_exp {
  factory Input$String_comparison_exp({
    String? $_eq,
    String? $_gt,
    String? $_gte,
    String? $_ilike,
    List<String>? $_in,
    String? $_iregex,
    bool? $_is_null,
    String? $_like,
    String? $_lt,
    String? $_lte,
    String? $_neq,
    String? $_nilike,
    List<String>? $_nin,
    String? $_niregex,
    String? $_nlike,
    String? $_nregex,
    String? $_nsimilar,
    String? $_regex,
    String? $_similar,
  }) =>
      Input$String_comparison_exp._({
        if ($_eq != null) r'_eq': $_eq,
        if ($_gt != null) r'_gt': $_gt,
        if ($_gte != null) r'_gte': $_gte,
        if ($_ilike != null) r'_ilike': $_ilike,
        if ($_in != null) r'_in': $_in,
        if ($_iregex != null) r'_iregex': $_iregex,
        if ($_is_null != null) r'_is_null': $_is_null,
        if ($_like != null) r'_like': $_like,
        if ($_lt != null) r'_lt': $_lt,
        if ($_lte != null) r'_lte': $_lte,
        if ($_neq != null) r'_neq': $_neq,
        if ($_nilike != null) r'_nilike': $_nilike,
        if ($_nin != null) r'_nin': $_nin,
        if ($_niregex != null) r'_niregex': $_niregex,
        if ($_nlike != null) r'_nlike': $_nlike,
        if ($_nregex != null) r'_nregex': $_nregex,
        if ($_nsimilar != null) r'_nsimilar': $_nsimilar,
        if ($_regex != null) r'_regex': $_regex,
        if ($_similar != null) r'_similar': $_similar,
      });

  Input$String_comparison_exp._(this._$data);

  factory Input$String_comparison_exp.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    if (data.containsKey('_eq')) {
      final l$$_eq = data['_eq'];
      result$data['_eq'] = (l$$_eq as String?);
    }
    if (data.containsKey('_gt')) {
      final l$$_gt = data['_gt'];
      result$data['_gt'] = (l$$_gt as String?);
    }
    if (data.containsKey('_gte')) {
      final l$$_gte = data['_gte'];
      result$data['_gte'] = (l$$_gte as String?);
    }
    if (data.containsKey('_ilike')) {
      final l$$_ilike = data['_ilike'];
      result$data['_ilike'] = (l$$_ilike as String?);
    }
    if (data.containsKey('_in')) {
      final l$$_in = data['_in'];
      result$data['_in'] =
          (l$$_in as List<dynamic>?)?.map((e) => (e as String)).toList();
    }
    if (data.containsKey('_iregex')) {
      final l$$_iregex = data['_iregex'];
      result$data['_iregex'] = (l$$_iregex as String?);
    }
    if (data.containsKey('_is_null')) {
      final l$$_is_null = data['_is_null'];
      result$data['_is_null'] = (l$$_is_null as bool?);
    }
    if (data.containsKey('_like')) {
      final l$$_like = data['_like'];
      result$data['_like'] = (l$$_like as String?);
    }
    if (data.containsKey('_lt')) {
      final l$$_lt = data['_lt'];
      result$data['_lt'] = (l$$_lt as String?);
    }
    if (data.containsKey('_lte')) {
      final l$$_lte = data['_lte'];
      result$data['_lte'] = (l$$_lte as String?);
    }
    if (data.containsKey('_neq')) {
      final l$$_neq = data['_neq'];
      result$data['_neq'] = (l$$_neq as String?);
    }
    if (data.containsKey('_nilike')) {
      final l$$_nilike = data['_nilike'];
      result$data['_nilike'] = (l$$_nilike as String?);
    }
    if (data.containsKey('_nin')) {
      final l$$_nin = data['_nin'];
      result$data['_nin'] =
          (l$$_nin as List<dynamic>?)?.map((e) => (e as String)).toList();
    }
    if (data.containsKey('_niregex')) {
      final l$$_niregex = data['_niregex'];
      result$data['_niregex'] = (l$$_niregex as String?);
    }
    if (data.containsKey('_nlike')) {
      final l$$_nlike = data['_nlike'];
      result$data['_nlike'] = (l$$_nlike as String?);
    }
    if (data.containsKey('_nregex')) {
      final l$$_nregex = data['_nregex'];
      result$data['_nregex'] = (l$$_nregex as String?);
    }
    if (data.containsKey('_nsimilar')) {
      final l$$_nsimilar = data['_nsimilar'];
      result$data['_nsimilar'] = (l$$_nsimilar as String?);
    }
    if (data.containsKey('_regex')) {
      final l$$_regex = data['_regex'];
      result$data['_regex'] = (l$$_regex as String?);
    }
    if (data.containsKey('_similar')) {
      final l$$_similar = data['_similar'];
      result$data['_similar'] = (l$$_similar as String?);
    }
    return Input$String_comparison_exp._(result$data);
  }

  Map<String, dynamic> _$data;

  String? get $_eq => (_$data['_eq'] as String?);

  String? get $_gt => (_$data['_gt'] as String?);

  String? get $_gte => (_$data['_gte'] as String?);

  String? get $_ilike => (_$data['_ilike'] as String?);

  List<String>? get $_in => (_$data['_in'] as List<String>?);

  String? get $_iregex => (_$data['_iregex'] as String?);

  bool? get $_is_null => (_$data['_is_null'] as bool?);

  String? get $_like => (_$data['_like'] as String?);

  String? get $_lt => (_$data['_lt'] as String?);

  String? get $_lte => (_$data['_lte'] as String?);

  String? get $_neq => (_$data['_neq'] as String?);

  String? get $_nilike => (_$data['_nilike'] as String?);

  List<String>? get $_nin => (_$data['_nin'] as List<String>?);

  String? get $_niregex => (_$data['_niregex'] as String?);

  String? get $_nlike => (_$data['_nlike'] as String?);

  String? get $_nregex => (_$data['_nregex'] as String?);

  String? get $_nsimilar => (_$data['_nsimilar'] as String?);

  String? get $_regex => (_$data['_regex'] as String?);

  String? get $_similar => (_$data['_similar'] as String?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    if (_$data.containsKey('_eq')) {
      final l$$_eq = $_eq;
      result$data['_eq'] = l$$_eq;
    }
    if (_$data.containsKey('_gt')) {
      final l$$_gt = $_gt;
      result$data['_gt'] = l$$_gt;
    }
    if (_$data.containsKey('_gte')) {
      final l$$_gte = $_gte;
      result$data['_gte'] = l$$_gte;
    }
    if (_$data.containsKey('_ilike')) {
      final l$$_ilike = $_ilike;
      result$data['_ilike'] = l$$_ilike;
    }
    if (_$data.containsKey('_in')) {
      final l$$_in = $_in;
      result$data['_in'] = l$$_in?.map((e) => e).toList();
    }
    if (_$data.containsKey('_iregex')) {
      final l$$_iregex = $_iregex;
      result$data['_iregex'] = l$$_iregex;
    }
    if (_$data.containsKey('_is_null')) {
      final l$$_is_null = $_is_null;
      result$data['_is_null'] = l$$_is_null;
    }
    if (_$data.containsKey('_like')) {
      final l$$_like = $_like;
      result$data['_like'] = l$$_like;
    }
    if (_$data.containsKey('_lt')) {
      final l$$_lt = $_lt;
      result$data['_lt'] = l$$_lt;
    }
    if (_$data.containsKey('_lte')) {
      final l$$_lte = $_lte;
      result$data['_lte'] = l$$_lte;
    }
    if (_$data.containsKey('_neq')) {
      final l$$_neq = $_neq;
      result$data['_neq'] = l$$_neq;
    }
    if (_$data.containsKey('_nilike')) {
      final l$$_nilike = $_nilike;
      result$data['_nilike'] = l$$_nilike;
    }
    if (_$data.containsKey('_nin')) {
      final l$$_nin = $_nin;
      result$data['_nin'] = l$$_nin?.map((e) => e).toList();
    }
    if (_$data.containsKey('_niregex')) {
      final l$$_niregex = $_niregex;
      result$data['_niregex'] = l$$_niregex;
    }
    if (_$data.containsKey('_nlike')) {
      final l$$_nlike = $_nlike;
      result$data['_nlike'] = l$$_nlike;
    }
    if (_$data.containsKey('_nregex')) {
      final l$$_nregex = $_nregex;
      result$data['_nregex'] = l$$_nregex;
    }
    if (_$data.containsKey('_nsimilar')) {
      final l$$_nsimilar = $_nsimilar;
      result$data['_nsimilar'] = l$$_nsimilar;
    }
    if (_$data.containsKey('_regex')) {
      final l$$_regex = $_regex;
      result$data['_regex'] = l$$_regex;
    }
    if (_$data.containsKey('_similar')) {
      final l$$_similar = $_similar;
      result$data['_similar'] = l$$_similar;
    }
    return result$data;
  }

  CopyWith$Input$String_comparison_exp<Input$String_comparison_exp>
      get copyWith => CopyWith$Input$String_comparison_exp(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$String_comparison_exp) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$$_eq = $_eq;
    final lOther$$_eq = other.$_eq;
    if (_$data.containsKey('_eq') != other._$data.containsKey('_eq')) {
      return false;
    }
    if (l$$_eq != lOther$$_eq) {
      return false;
    }
    final l$$_gt = $_gt;
    final lOther$$_gt = other.$_gt;
    if (_$data.containsKey('_gt') != other._$data.containsKey('_gt')) {
      return false;
    }
    if (l$$_gt != lOther$$_gt) {
      return false;
    }
    final l$$_gte = $_gte;
    final lOther$$_gte = other.$_gte;
    if (_$data.containsKey('_gte') != other._$data.containsKey('_gte')) {
      return false;
    }
    if (l$$_gte != lOther$$_gte) {
      return false;
    }
    final l$$_ilike = $_ilike;
    final lOther$$_ilike = other.$_ilike;
    if (_$data.containsKey('_ilike') != other._$data.containsKey('_ilike')) {
      return false;
    }
    if (l$$_ilike != lOther$$_ilike) {
      return false;
    }
    final l$$_in = $_in;
    final lOther$$_in = other.$_in;
    if (_$data.containsKey('_in') != other._$data.containsKey('_in')) {
      return false;
    }
    if (l$$_in != null && lOther$$_in != null) {
      if (l$$_in.length != lOther$$_in.length) {
        return false;
      }
      for (int i = 0; i < l$$_in.length; i++) {
        final l$$_in$entry = l$$_in[i];
        final lOther$$_in$entry = lOther$$_in[i];
        if (l$$_in$entry != lOther$$_in$entry) {
          return false;
        }
      }
    } else if (l$$_in != lOther$$_in) {
      return false;
    }
    final l$$_iregex = $_iregex;
    final lOther$$_iregex = other.$_iregex;
    if (_$data.containsKey('_iregex') != other._$data.containsKey('_iregex')) {
      return false;
    }
    if (l$$_iregex != lOther$$_iregex) {
      return false;
    }
    final l$$_is_null = $_is_null;
    final lOther$$_is_null = other.$_is_null;
    if (_$data.containsKey('_is_null') !=
        other._$data.containsKey('_is_null')) {
      return false;
    }
    if (l$$_is_null != lOther$$_is_null) {
      return false;
    }
    final l$$_like = $_like;
    final lOther$$_like = other.$_like;
    if (_$data.containsKey('_like') != other._$data.containsKey('_like')) {
      return false;
    }
    if (l$$_like != lOther$$_like) {
      return false;
    }
    final l$$_lt = $_lt;
    final lOther$$_lt = other.$_lt;
    if (_$data.containsKey('_lt') != other._$data.containsKey('_lt')) {
      return false;
    }
    if (l$$_lt != lOther$$_lt) {
      return false;
    }
    final l$$_lte = $_lte;
    final lOther$$_lte = other.$_lte;
    if (_$data.containsKey('_lte') != other._$data.containsKey('_lte')) {
      return false;
    }
    if (l$$_lte != lOther$$_lte) {
      return false;
    }
    final l$$_neq = $_neq;
    final lOther$$_neq = other.$_neq;
    if (_$data.containsKey('_neq') != other._$data.containsKey('_neq')) {
      return false;
    }
    if (l$$_neq != lOther$$_neq) {
      return false;
    }
    final l$$_nilike = $_nilike;
    final lOther$$_nilike = other.$_nilike;
    if (_$data.containsKey('_nilike') != other._$data.containsKey('_nilike')) {
      return false;
    }
    if (l$$_nilike != lOther$$_nilike) {
      return false;
    }
    final l$$_nin = $_nin;
    final lOther$$_nin = other.$_nin;
    if (_$data.containsKey('_nin') != other._$data.containsKey('_nin')) {
      return false;
    }
    if (l$$_nin != null && lOther$$_nin != null) {
      if (l$$_nin.length != lOther$$_nin.length) {
        return false;
      }
      for (int i = 0; i < l$$_nin.length; i++) {
        final l$$_nin$entry = l$$_nin[i];
        final lOther$$_nin$entry = lOther$$_nin[i];
        if (l$$_nin$entry != lOther$$_nin$entry) {
          return false;
        }
      }
    } else if (l$$_nin != lOther$$_nin) {
      return false;
    }
    final l$$_niregex = $_niregex;
    final lOther$$_niregex = other.$_niregex;
    if (_$data.containsKey('_niregex') !=
        other._$data.containsKey('_niregex')) {
      return false;
    }
    if (l$$_niregex != lOther$$_niregex) {
      return false;
    }
    final l$$_nlike = $_nlike;
    final lOther$$_nlike = other.$_nlike;
    if (_$data.containsKey('_nlike') != other._$data.containsKey('_nlike')) {
      return false;
    }
    if (l$$_nlike != lOther$$_nlike) {
      return false;
    }
    final l$$_nregex = $_nregex;
    final lOther$$_nregex = other.$_nregex;
    if (_$data.containsKey('_nregex') != other._$data.containsKey('_nregex')) {
      return false;
    }
    if (l$$_nregex != lOther$$_nregex) {
      return false;
    }
    final l$$_nsimilar = $_nsimilar;
    final lOther$$_nsimilar = other.$_nsimilar;
    if (_$data.containsKey('_nsimilar') !=
        other._$data.containsKey('_nsimilar')) {
      return false;
    }
    if (l$$_nsimilar != lOther$$_nsimilar) {
      return false;
    }
    final l$$_regex = $_regex;
    final lOther$$_regex = other.$_regex;
    if (_$data.containsKey('_regex') != other._$data.containsKey('_regex')) {
      return false;
    }
    if (l$$_regex != lOther$$_regex) {
      return false;
    }
    final l$$_similar = $_similar;
    final lOther$$_similar = other.$_similar;
    if (_$data.containsKey('_similar') !=
        other._$data.containsKey('_similar')) {
      return false;
    }
    if (l$$_similar != lOther$$_similar) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$$_eq = $_eq;
    final l$$_gt = $_gt;
    final l$$_gte = $_gte;
    final l$$_ilike = $_ilike;
    final l$$_in = $_in;
    final l$$_iregex = $_iregex;
    final l$$_is_null = $_is_null;
    final l$$_like = $_like;
    final l$$_lt = $_lt;
    final l$$_lte = $_lte;
    final l$$_neq = $_neq;
    final l$$_nilike = $_nilike;
    final l$$_nin = $_nin;
    final l$$_niregex = $_niregex;
    final l$$_nlike = $_nlike;
    final l$$_nregex = $_nregex;
    final l$$_nsimilar = $_nsimilar;
    final l$$_regex = $_regex;
    final l$$_similar = $_similar;
    return Object.hashAll([
      _$data.containsKey('_eq') ? l$$_eq : const {},
      _$data.containsKey('_gt') ? l$$_gt : const {},
      _$data.containsKey('_gte') ? l$$_gte : const {},
      _$data.containsKey('_ilike') ? l$$_ilike : const {},
      _$data.containsKey('_in')
          ? l$$_in == null
              ? null
              : Object.hashAll(l$$_in.map((v) => v))
          : const {},
      _$data.containsKey('_iregex') ? l$$_iregex : const {},
      _$data.containsKey('_is_null') ? l$$_is_null : const {},
      _$data.containsKey('_like') ? l$$_like : const {},
      _$data.containsKey('_lt') ? l$$_lt : const {},
      _$data.containsKey('_lte') ? l$$_lte : const {},
      _$data.containsKey('_neq') ? l$$_neq : const {},
      _$data.containsKey('_nilike') ? l$$_nilike : const {},
      _$data.containsKey('_nin')
          ? l$$_nin == null
              ? null
              : Object.hashAll(l$$_nin.map((v) => v))
          : const {},
      _$data.containsKey('_niregex') ? l$$_niregex : const {},
      _$data.containsKey('_nlike') ? l$$_nlike : const {},
      _$data.containsKey('_nregex') ? l$$_nregex : const {},
      _$data.containsKey('_nsimilar') ? l$$_nsimilar : const {},
      _$data.containsKey('_regex') ? l$$_regex : const {},
      _$data.containsKey('_similar') ? l$$_similar : const {},
    ]);
  }
}

abstract class CopyWith$Input$String_comparison_exp<TRes> {
  factory CopyWith$Input$String_comparison_exp(
    Input$String_comparison_exp instance,
    TRes Function(Input$String_comparison_exp) then,
  ) = _CopyWithImpl$Input$String_comparison_exp;

  factory CopyWith$Input$String_comparison_exp.stub(TRes res) =
      _CopyWithStubImpl$Input$String_comparison_exp;

  TRes call({
    String? $_eq,
    String? $_gt,
    String? $_gte,
    String? $_ilike,
    List<String>? $_in,
    String? $_iregex,
    bool? $_is_null,
    String? $_like,
    String? $_lt,
    String? $_lte,
    String? $_neq,
    String? $_nilike,
    List<String>? $_nin,
    String? $_niregex,
    String? $_nlike,
    String? $_nregex,
    String? $_nsimilar,
    String? $_regex,
    String? $_similar,
  });
}

class _CopyWithImpl$Input$String_comparison_exp<TRes>
    implements CopyWith$Input$String_comparison_exp<TRes> {
  _CopyWithImpl$Input$String_comparison_exp(
    this._instance,
    this._then,
  );

  final Input$String_comparison_exp _instance;

  final TRes Function(Input$String_comparison_exp) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? $_eq = _undefined,
    Object? $_gt = _undefined,
    Object? $_gte = _undefined,
    Object? $_ilike = _undefined,
    Object? $_in = _undefined,
    Object? $_iregex = _undefined,
    Object? $_is_null = _undefined,
    Object? $_like = _undefined,
    Object? $_lt = _undefined,
    Object? $_lte = _undefined,
    Object? $_neq = _undefined,
    Object? $_nilike = _undefined,
    Object? $_nin = _undefined,
    Object? $_niregex = _undefined,
    Object? $_nlike = _undefined,
    Object? $_nregex = _undefined,
    Object? $_nsimilar = _undefined,
    Object? $_regex = _undefined,
    Object? $_similar = _undefined,
  }) =>
      _then(Input$String_comparison_exp._({
        ..._instance._$data,
        if ($_eq != _undefined) '_eq': ($_eq as String?),
        if ($_gt != _undefined) '_gt': ($_gt as String?),
        if ($_gte != _undefined) '_gte': ($_gte as String?),
        if ($_ilike != _undefined) '_ilike': ($_ilike as String?),
        if ($_in != _undefined) '_in': ($_in as List<String>?),
        if ($_iregex != _undefined) '_iregex': ($_iregex as String?),
        if ($_is_null != _undefined) '_is_null': ($_is_null as bool?),
        if ($_like != _undefined) '_like': ($_like as String?),
        if ($_lt != _undefined) '_lt': ($_lt as String?),
        if ($_lte != _undefined) '_lte': ($_lte as String?),
        if ($_neq != _undefined) '_neq': ($_neq as String?),
        if ($_nilike != _undefined) '_nilike': ($_nilike as String?),
        if ($_nin != _undefined) '_nin': ($_nin as List<String>?),
        if ($_niregex != _undefined) '_niregex': ($_niregex as String?),
        if ($_nlike != _undefined) '_nlike': ($_nlike as String?),
        if ($_nregex != _undefined) '_nregex': ($_nregex as String?),
        if ($_nsimilar != _undefined) '_nsimilar': ($_nsimilar as String?),
        if ($_regex != _undefined) '_regex': ($_regex as String?),
        if ($_similar != _undefined) '_similar': ($_similar as String?),
      }));
}

class _CopyWithStubImpl$Input$String_comparison_exp<TRes>
    implements CopyWith$Input$String_comparison_exp<TRes> {
  _CopyWithStubImpl$Input$String_comparison_exp(this._res);

  TRes _res;

  call({
    String? $_eq,
    String? $_gt,
    String? $_gte,
    String? $_ilike,
    List<String>? $_in,
    String? $_iregex,
    bool? $_is_null,
    String? $_like,
    String? $_lt,
    String? $_lte,
    String? $_neq,
    String? $_nilike,
    List<String>? $_nin,
    String? $_niregex,
    String? $_nlike,
    String? $_nregex,
    String? $_nsimilar,
    String? $_regex,
    String? $_similar,
  }) =>
      _res;
}

class Input$timestamptz_comparison_exp {
  factory Input$timestamptz_comparison_exp({
    DateTime? $_eq,
    DateTime? $_gt,
    DateTime? $_gte,
    List<DateTime>? $_in,
    bool? $_is_null,
    DateTime? $_lt,
    DateTime? $_lte,
    DateTime? $_neq,
    List<DateTime>? $_nin,
  }) =>
      Input$timestamptz_comparison_exp._({
        if ($_eq != null) r'_eq': $_eq,
        if ($_gt != null) r'_gt': $_gt,
        if ($_gte != null) r'_gte': $_gte,
        if ($_in != null) r'_in': $_in,
        if ($_is_null != null) r'_is_null': $_is_null,
        if ($_lt != null) r'_lt': $_lt,
        if ($_lte != null) r'_lte': $_lte,
        if ($_neq != null) r'_neq': $_neq,
        if ($_nin != null) r'_nin': $_nin,
      });

  Input$timestamptz_comparison_exp._(this._$data);

  factory Input$timestamptz_comparison_exp.fromJson(Map<String, dynamic> data) {
    final result$data = <String, dynamic>{};
    if (data.containsKey('_eq')) {
      final l$$_eq = data['_eq'];
      result$data['_eq'] =
          l$$_eq == null ? null : DateTime.parse((l$$_eq as String));
    }
    if (data.containsKey('_gt')) {
      final l$$_gt = data['_gt'];
      result$data['_gt'] =
          l$$_gt == null ? null : DateTime.parse((l$$_gt as String));
    }
    if (data.containsKey('_gte')) {
      final l$$_gte = data['_gte'];
      result$data['_gte'] =
          l$$_gte == null ? null : DateTime.parse((l$$_gte as String));
    }
    if (data.containsKey('_in')) {
      final l$$_in = data['_in'];
      result$data['_in'] = (l$$_in as List<dynamic>?)
          ?.map((e) => DateTime.parse((e as String)))
          .toList();
    }
    if (data.containsKey('_is_null')) {
      final l$$_is_null = data['_is_null'];
      result$data['_is_null'] = (l$$_is_null as bool?);
    }
    if (data.containsKey('_lt')) {
      final l$$_lt = data['_lt'];
      result$data['_lt'] =
          l$$_lt == null ? null : DateTime.parse((l$$_lt as String));
    }
    if (data.containsKey('_lte')) {
      final l$$_lte = data['_lte'];
      result$data['_lte'] =
          l$$_lte == null ? null : DateTime.parse((l$$_lte as String));
    }
    if (data.containsKey('_neq')) {
      final l$$_neq = data['_neq'];
      result$data['_neq'] =
          l$$_neq == null ? null : DateTime.parse((l$$_neq as String));
    }
    if (data.containsKey('_nin')) {
      final l$$_nin = data['_nin'];
      result$data['_nin'] = (l$$_nin as List<dynamic>?)
          ?.map((e) => DateTime.parse((e as String)))
          .toList();
    }
    return Input$timestamptz_comparison_exp._(result$data);
  }

  Map<String, dynamic> _$data;

  DateTime? get $_eq => (_$data['_eq'] as DateTime?);

  DateTime? get $_gt => (_$data['_gt'] as DateTime?);

  DateTime? get $_gte => (_$data['_gte'] as DateTime?);

  List<DateTime>? get $_in => (_$data['_in'] as List<DateTime>?);

  bool? get $_is_null => (_$data['_is_null'] as bool?);

  DateTime? get $_lt => (_$data['_lt'] as DateTime?);

  DateTime? get $_lte => (_$data['_lte'] as DateTime?);

  DateTime? get $_neq => (_$data['_neq'] as DateTime?);

  List<DateTime>? get $_nin => (_$data['_nin'] as List<DateTime>?);

  Map<String, dynamic> toJson() {
    final result$data = <String, dynamic>{};
    if (_$data.containsKey('_eq')) {
      final l$$_eq = $_eq;
      result$data['_eq'] = l$$_eq?.toIso8601String();
    }
    if (_$data.containsKey('_gt')) {
      final l$$_gt = $_gt;
      result$data['_gt'] = l$$_gt?.toIso8601String();
    }
    if (_$data.containsKey('_gte')) {
      final l$$_gte = $_gte;
      result$data['_gte'] = l$$_gte?.toIso8601String();
    }
    if (_$data.containsKey('_in')) {
      final l$$_in = $_in;
      result$data['_in'] = l$$_in?.map((e) => e.toIso8601String()).toList();
    }
    if (_$data.containsKey('_is_null')) {
      final l$$_is_null = $_is_null;
      result$data['_is_null'] = l$$_is_null;
    }
    if (_$data.containsKey('_lt')) {
      final l$$_lt = $_lt;
      result$data['_lt'] = l$$_lt?.toIso8601String();
    }
    if (_$data.containsKey('_lte')) {
      final l$$_lte = $_lte;
      result$data['_lte'] = l$$_lte?.toIso8601String();
    }
    if (_$data.containsKey('_neq')) {
      final l$$_neq = $_neq;
      result$data['_neq'] = l$$_neq?.toIso8601String();
    }
    if (_$data.containsKey('_nin')) {
      final l$$_nin = $_nin;
      result$data['_nin'] = l$$_nin?.map((e) => e.toIso8601String()).toList();
    }
    return result$data;
  }

  CopyWith$Input$timestamptz_comparison_exp<Input$timestamptz_comparison_exp>
      get copyWith => CopyWith$Input$timestamptz_comparison_exp(
            this,
            (i) => i,
          );

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }
    if (!(other is Input$timestamptz_comparison_exp) ||
        runtimeType != other.runtimeType) {
      return false;
    }
    final l$$_eq = $_eq;
    final lOther$$_eq = other.$_eq;
    if (_$data.containsKey('_eq') != other._$data.containsKey('_eq')) {
      return false;
    }
    if (l$$_eq != lOther$$_eq) {
      return false;
    }
    final l$$_gt = $_gt;
    final lOther$$_gt = other.$_gt;
    if (_$data.containsKey('_gt') != other._$data.containsKey('_gt')) {
      return false;
    }
    if (l$$_gt != lOther$$_gt) {
      return false;
    }
    final l$$_gte = $_gte;
    final lOther$$_gte = other.$_gte;
    if (_$data.containsKey('_gte') != other._$data.containsKey('_gte')) {
      return false;
    }
    if (l$$_gte != lOther$$_gte) {
      return false;
    }
    final l$$_in = $_in;
    final lOther$$_in = other.$_in;
    if (_$data.containsKey('_in') != other._$data.containsKey('_in')) {
      return false;
    }
    if (l$$_in != null && lOther$$_in != null) {
      if (l$$_in.length != lOther$$_in.length) {
        return false;
      }
      for (int i = 0; i < l$$_in.length; i++) {
        final l$$_in$entry = l$$_in[i];
        final lOther$$_in$entry = lOther$$_in[i];
        if (l$$_in$entry != lOther$$_in$entry) {
          return false;
        }
      }
    } else if (l$$_in != lOther$$_in) {
      return false;
    }
    final l$$_is_null = $_is_null;
    final lOther$$_is_null = other.$_is_null;
    if (_$data.containsKey('_is_null') !=
        other._$data.containsKey('_is_null')) {
      return false;
    }
    if (l$$_is_null != lOther$$_is_null) {
      return false;
    }
    final l$$_lt = $_lt;
    final lOther$$_lt = other.$_lt;
    if (_$data.containsKey('_lt') != other._$data.containsKey('_lt')) {
      return false;
    }
    if (l$$_lt != lOther$$_lt) {
      return false;
    }
    final l$$_lte = $_lte;
    final lOther$$_lte = other.$_lte;
    if (_$data.containsKey('_lte') != other._$data.containsKey('_lte')) {
      return false;
    }
    if (l$$_lte != lOther$$_lte) {
      return false;
    }
    final l$$_neq = $_neq;
    final lOther$$_neq = other.$_neq;
    if (_$data.containsKey('_neq') != other._$data.containsKey('_neq')) {
      return false;
    }
    if (l$$_neq != lOther$$_neq) {
      return false;
    }
    final l$$_nin = $_nin;
    final lOther$$_nin = other.$_nin;
    if (_$data.containsKey('_nin') != other._$data.containsKey('_nin')) {
      return false;
    }
    if (l$$_nin != null && lOther$$_nin != null) {
      if (l$$_nin.length != lOther$$_nin.length) {
        return false;
      }
      for (int i = 0; i < l$$_nin.length; i++) {
        final l$$_nin$entry = l$$_nin[i];
        final lOther$$_nin$entry = lOther$$_nin[i];
        if (l$$_nin$entry != lOther$$_nin$entry) {
          return false;
        }
      }
    } else if (l$$_nin != lOther$$_nin) {
      return false;
    }
    return true;
  }

  @override
  int get hashCode {
    final l$$_eq = $_eq;
    final l$$_gt = $_gt;
    final l$$_gte = $_gte;
    final l$$_in = $_in;
    final l$$_is_null = $_is_null;
    final l$$_lt = $_lt;
    final l$$_lte = $_lte;
    final l$$_neq = $_neq;
    final l$$_nin = $_nin;
    return Object.hashAll([
      _$data.containsKey('_eq') ? l$$_eq : const {},
      _$data.containsKey('_gt') ? l$$_gt : const {},
      _$data.containsKey('_gte') ? l$$_gte : const {},
      _$data.containsKey('_in')
          ? l$$_in == null
              ? null
              : Object.hashAll(l$$_in.map((v) => v))
          : const {},
      _$data.containsKey('_is_null') ? l$$_is_null : const {},
      _$data.containsKey('_lt') ? l$$_lt : const {},
      _$data.containsKey('_lte') ? l$$_lte : const {},
      _$data.containsKey('_neq') ? l$$_neq : const {},
      _$data.containsKey('_nin')
          ? l$$_nin == null
              ? null
              : Object.hashAll(l$$_nin.map((v) => v))
          : const {},
    ]);
  }
}

abstract class CopyWith$Input$timestamptz_comparison_exp<TRes> {
  factory CopyWith$Input$timestamptz_comparison_exp(
    Input$timestamptz_comparison_exp instance,
    TRes Function(Input$timestamptz_comparison_exp) then,
  ) = _CopyWithImpl$Input$timestamptz_comparison_exp;

  factory CopyWith$Input$timestamptz_comparison_exp.stub(TRes res) =
      _CopyWithStubImpl$Input$timestamptz_comparison_exp;

  TRes call({
    DateTime? $_eq,
    DateTime? $_gt,
    DateTime? $_gte,
    List<DateTime>? $_in,
    bool? $_is_null,
    DateTime? $_lt,
    DateTime? $_lte,
    DateTime? $_neq,
    List<DateTime>? $_nin,
  });
}

class _CopyWithImpl$Input$timestamptz_comparison_exp<TRes>
    implements CopyWith$Input$timestamptz_comparison_exp<TRes> {
  _CopyWithImpl$Input$timestamptz_comparison_exp(
    this._instance,
    this._then,
  );

  final Input$timestamptz_comparison_exp _instance;

  final TRes Function(Input$timestamptz_comparison_exp) _then;

  static const _undefined = <dynamic, dynamic>{};

  TRes call({
    Object? $_eq = _undefined,
    Object? $_gt = _undefined,
    Object? $_gte = _undefined,
    Object? $_in = _undefined,
    Object? $_is_null = _undefined,
    Object? $_lt = _undefined,
    Object? $_lte = _undefined,
    Object? $_neq = _undefined,
    Object? $_nin = _undefined,
  }) =>
      _then(Input$timestamptz_comparison_exp._({
        ..._instance._$data,
        if ($_eq != _undefined) '_eq': ($_eq as DateTime?),
        if ($_gt != _undefined) '_gt': ($_gt as DateTime?),
        if ($_gte != _undefined) '_gte': ($_gte as DateTime?),
        if ($_in != _undefined) '_in': ($_in as List<DateTime>?),
        if ($_is_null != _undefined) '_is_null': ($_is_null as bool?),
        if ($_lt != _undefined) '_lt': ($_lt as DateTime?),
        if ($_lte != _undefined) '_lte': ($_lte as DateTime?),
        if ($_neq != _undefined) '_neq': ($_neq as DateTime?),
        if ($_nin != _undefined) '_nin': ($_nin as List<DateTime>?),
      }));
}

class _CopyWithStubImpl$Input$timestamptz_comparison_exp<TRes>
    implements CopyWith$Input$timestamptz_comparison_exp<TRes> {
  _CopyWithStubImpl$Input$timestamptz_comparison_exp(this._res);

  TRes _res;

  call({
    DateTime? $_eq,
    DateTime? $_gt,
    DateTime? $_gte,
    List<DateTime>? $_in,
    bool? $_is_null,
    DateTime? $_lt,
    DateTime? $_lte,
    DateTime? $_neq,
    List<DateTime>? $_nin,
  }) =>
      _res;
}

enum Enum$cursor_ordering {
  ASC,
  DESC,
  $unknown;

  factory Enum$cursor_ordering.fromJson(String value) =>
      fromJson$Enum$cursor_ordering(value);

  String toJson() => toJson$Enum$cursor_ordering(this);
}

String toJson$Enum$cursor_ordering(Enum$cursor_ordering e) {
  switch (e) {
    case Enum$cursor_ordering.ASC:
      return r'ASC';
    case Enum$cursor_ordering.DESC:
      return r'DESC';
    case Enum$cursor_ordering.$unknown:
      return r'$unknown';
  }
}

Enum$cursor_ordering fromJson$Enum$cursor_ordering(String value) {
  switch (value) {
    case r'ASC':
      return Enum$cursor_ordering.ASC;
    case r'DESC':
      return Enum$cursor_ordering.DESC;
    default:
      return Enum$cursor_ordering.$unknown;
  }
}

enum Enum$order_by {
  asc,
  asc_nulls_first,
  asc_nulls_last,
  desc,
  desc_nulls_first,
  desc_nulls_last,
  $unknown;

  factory Enum$order_by.fromJson(String value) => fromJson$Enum$order_by(value);

  String toJson() => toJson$Enum$order_by(this);
}

String toJson$Enum$order_by(Enum$order_by e) {
  switch (e) {
    case Enum$order_by.asc:
      return r'asc';
    case Enum$order_by.asc_nulls_first:
      return r'asc_nulls_first';
    case Enum$order_by.asc_nulls_last:
      return r'asc_nulls_last';
    case Enum$order_by.desc:
      return r'desc';
    case Enum$order_by.desc_nulls_first:
      return r'desc_nulls_first';
    case Enum$order_by.desc_nulls_last:
      return r'desc_nulls_last';
    case Enum$order_by.$unknown:
      return r'$unknown';
  }
}

Enum$order_by fromJson$Enum$order_by(String value) {
  switch (value) {
    case r'asc':
      return Enum$order_by.asc;
    case r'asc_nulls_first':
      return Enum$order_by.asc_nulls_first;
    case r'asc_nulls_last':
      return Enum$order_by.asc_nulls_last;
    case r'desc':
      return Enum$order_by.desc;
    case r'desc_nulls_first':
      return Enum$order_by.desc_nulls_first;
    case r'desc_nulls_last':
      return Enum$order_by.desc_nulls_last;
    default:
      return Enum$order_by.$unknown;
  }
}

enum Enum$profiles_select_column {
  avatar,
  city,
  data_cid,
  description,
  geoloc,
  index_request_cid,
  pubkey,
  socials,
  time,
  title,
  $unknown;

  factory Enum$profiles_select_column.fromJson(String value) =>
      fromJson$Enum$profiles_select_column(value);

  String toJson() => toJson$Enum$profiles_select_column(this);
}

String toJson$Enum$profiles_select_column(Enum$profiles_select_column e) {
  switch (e) {
    case Enum$profiles_select_column.avatar:
      return r'avatar';
    case Enum$profiles_select_column.city:
      return r'city';
    case Enum$profiles_select_column.data_cid:
      return r'data_cid';
    case Enum$profiles_select_column.description:
      return r'description';
    case Enum$profiles_select_column.geoloc:
      return r'geoloc';
    case Enum$profiles_select_column.index_request_cid:
      return r'index_request_cid';
    case Enum$profiles_select_column.pubkey:
      return r'pubkey';
    case Enum$profiles_select_column.socials:
      return r'socials';
    case Enum$profiles_select_column.time:
      return r'time';
    case Enum$profiles_select_column.title:
      return r'title';
    case Enum$profiles_select_column.$unknown:
      return r'$unknown';
  }
}

Enum$profiles_select_column fromJson$Enum$profiles_select_column(String value) {
  switch (value) {
    case r'avatar':
      return Enum$profiles_select_column.avatar;
    case r'city':
      return Enum$profiles_select_column.city;
    case r'data_cid':
      return Enum$profiles_select_column.data_cid;
    case r'description':
      return Enum$profiles_select_column.description;
    case r'geoloc':
      return Enum$profiles_select_column.geoloc;
    case r'index_request_cid':
      return Enum$profiles_select_column.index_request_cid;
    case r'pubkey':
      return Enum$profiles_select_column.pubkey;
    case r'socials':
      return Enum$profiles_select_column.socials;
    case r'time':
      return Enum$profiles_select_column.time;
    case r'title':
      return Enum$profiles_select_column.title;
    default:
      return Enum$profiles_select_column.$unknown;
  }
}

enum Enum$__TypeKind {
  SCALAR,
  OBJECT,
  INTERFACE,
  UNION,
  ENUM,
  INPUT_OBJECT,
  LIST,
  NON_NULL,
  $unknown;

  factory Enum$__TypeKind.fromJson(String value) =>
      fromJson$Enum$__TypeKind(value);

  String toJson() => toJson$Enum$__TypeKind(this);
}

String toJson$Enum$__TypeKind(Enum$__TypeKind e) {
  switch (e) {
    case Enum$__TypeKind.SCALAR:
      return r'SCALAR';
    case Enum$__TypeKind.OBJECT:
      return r'OBJECT';
    case Enum$__TypeKind.INTERFACE:
      return r'INTERFACE';
    case Enum$__TypeKind.UNION:
      return r'UNION';
    case Enum$__TypeKind.ENUM:
      return r'ENUM';
    case Enum$__TypeKind.INPUT_OBJECT:
      return r'INPUT_OBJECT';
    case Enum$__TypeKind.LIST:
      return r'LIST';
    case Enum$__TypeKind.NON_NULL:
      return r'NON_NULL';
    case Enum$__TypeKind.$unknown:
      return r'$unknown';
  }
}

Enum$__TypeKind fromJson$Enum$__TypeKind(String value) {
  switch (value) {
    case r'SCALAR':
      return Enum$__TypeKind.SCALAR;
    case r'OBJECT':
      return Enum$__TypeKind.OBJECT;
    case r'INTERFACE':
      return Enum$__TypeKind.INTERFACE;
    case r'UNION':
      return Enum$__TypeKind.UNION;
    case r'ENUM':
      return Enum$__TypeKind.ENUM;
    case r'INPUT_OBJECT':
      return Enum$__TypeKind.INPUT_OBJECT;
    case r'LIST':
      return Enum$__TypeKind.LIST;
    case r'NON_NULL':
      return Enum$__TypeKind.NON_NULL;
    default:
      return Enum$__TypeKind.$unknown;
  }
}

enum Enum$__DirectiveLocation {
  QUERY,
  MUTATION,
  SUBSCRIPTION,
  FIELD,
  FRAGMENT_DEFINITION,
  FRAGMENT_SPREAD,
  INLINE_FRAGMENT,
  VARIABLE_DEFINITION,
  SCHEMA,
  SCALAR,
  OBJECT,
  FIELD_DEFINITION,
  ARGUMENT_DEFINITION,
  INTERFACE,
  UNION,
  ENUM,
  ENUM_VALUE,
  INPUT_OBJECT,
  INPUT_FIELD_DEFINITION,
  $unknown;

  factory Enum$__DirectiveLocation.fromJson(String value) =>
      fromJson$Enum$__DirectiveLocation(value);

  String toJson() => toJson$Enum$__DirectiveLocation(this);
}

String toJson$Enum$__DirectiveLocation(Enum$__DirectiveLocation e) {
  switch (e) {
    case Enum$__DirectiveLocation.QUERY:
      return r'QUERY';
    case Enum$__DirectiveLocation.MUTATION:
      return r'MUTATION';
    case Enum$__DirectiveLocation.SUBSCRIPTION:
      return r'SUBSCRIPTION';
    case Enum$__DirectiveLocation.FIELD:
      return r'FIELD';
    case Enum$__DirectiveLocation.FRAGMENT_DEFINITION:
      return r'FRAGMENT_DEFINITION';
    case Enum$__DirectiveLocation.FRAGMENT_SPREAD:
      return r'FRAGMENT_SPREAD';
    case Enum$__DirectiveLocation.INLINE_FRAGMENT:
      return r'INLINE_FRAGMENT';
    case Enum$__DirectiveLocation.VARIABLE_DEFINITION:
      return r'VARIABLE_DEFINITION';
    case Enum$__DirectiveLocation.SCHEMA:
      return r'SCHEMA';
    case Enum$__DirectiveLocation.SCALAR:
      return r'SCALAR';
    case Enum$__DirectiveLocation.OBJECT:
      return r'OBJECT';
    case Enum$__DirectiveLocation.FIELD_DEFINITION:
      return r'FIELD_DEFINITION';
    case Enum$__DirectiveLocation.ARGUMENT_DEFINITION:
      return r'ARGUMENT_DEFINITION';
    case Enum$__DirectiveLocation.INTERFACE:
      return r'INTERFACE';
    case Enum$__DirectiveLocation.UNION:
      return r'UNION';
    case Enum$__DirectiveLocation.ENUM:
      return r'ENUM';
    case Enum$__DirectiveLocation.ENUM_VALUE:
      return r'ENUM_VALUE';
    case Enum$__DirectiveLocation.INPUT_OBJECT:
      return r'INPUT_OBJECT';
    case Enum$__DirectiveLocation.INPUT_FIELD_DEFINITION:
      return r'INPUT_FIELD_DEFINITION';
    case Enum$__DirectiveLocation.$unknown:
      return r'$unknown';
  }
}

Enum$__DirectiveLocation fromJson$Enum$__DirectiveLocation(String value) {
  switch (value) {
    case r'QUERY':
      return Enum$__DirectiveLocation.QUERY;
    case r'MUTATION':
      return Enum$__DirectiveLocation.MUTATION;
    case r'SUBSCRIPTION':
      return Enum$__DirectiveLocation.SUBSCRIPTION;
    case r'FIELD':
      return Enum$__DirectiveLocation.FIELD;
    case r'FRAGMENT_DEFINITION':
      return Enum$__DirectiveLocation.FRAGMENT_DEFINITION;
    case r'FRAGMENT_SPREAD':
      return Enum$__DirectiveLocation.FRAGMENT_SPREAD;
    case r'INLINE_FRAGMENT':
      return Enum$__DirectiveLocation.INLINE_FRAGMENT;
    case r'VARIABLE_DEFINITION':
      return Enum$__DirectiveLocation.VARIABLE_DEFINITION;
    case r'SCHEMA':
      return Enum$__DirectiveLocation.SCHEMA;
    case r'SCALAR':
      return Enum$__DirectiveLocation.SCALAR;
    case r'OBJECT':
      return Enum$__DirectiveLocation.OBJECT;
    case r'FIELD_DEFINITION':
      return Enum$__DirectiveLocation.FIELD_DEFINITION;
    case r'ARGUMENT_DEFINITION':
      return Enum$__DirectiveLocation.ARGUMENT_DEFINITION;
    case r'INTERFACE':
      return Enum$__DirectiveLocation.INTERFACE;
    case r'UNION':
      return Enum$__DirectiveLocation.UNION;
    case r'ENUM':
      return Enum$__DirectiveLocation.ENUM;
    case r'ENUM_VALUE':
      return Enum$__DirectiveLocation.ENUM_VALUE;
    case r'INPUT_OBJECT':
      return Enum$__DirectiveLocation.INPUT_OBJECT;
    case r'INPUT_FIELD_DEFINITION':
      return Enum$__DirectiveLocation.INPUT_FIELD_DEFINITION;
    default:
      return Enum$__DirectiveLocation.$unknown;
  }
}

const possibleTypesMap = <String, Set<String>>{
  'Node': {
    'Account',
    'Block',
    'Call',
    'Cert',
    'CertEvent',
    'ChangeOwnerKey',
    'Event',
    'Extrinsic',
    'Identity',
    'ItemsCounter',
    'MembershipEvent',
    'SmithCert',
    'Transfer',
    'UdHistory',
    'UdReeval',
    'UniversalDividend',
  }
};
