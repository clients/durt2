import 'dart:isolate';
import 'dart:typed_data';

class RetrieveMnemonicArgs {
  final int safeBoxNumber;
  final SendPort sendPort;
  final Uint8List encryptionKey;
  final String appDir;

  RetrieveMnemonicArgs({
    required this.safeBoxNumber,
    required this.sendPort,
    required this.encryptionKey,
    required this.appDir,
  });

  @override
  String toString() {
    return """RetrieveMnemonicArgs{
      safeBoxNumber: $safeBoxNumber,
      sendPort: $sendPort,
      encryptionKey: $encryptionKey,
      appDocumentPath: $appDir
      }""";
  }
}
