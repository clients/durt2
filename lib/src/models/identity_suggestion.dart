class IdentitySuggestion {
  final String name;
  final String address;

  IdentitySuggestion({required this.name, required this.address});
}
