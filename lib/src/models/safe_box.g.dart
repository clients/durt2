// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'safe_box.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SafeBoxAdapter extends TypeAdapter<SafeBox> {
  @override
  final int typeId = 4;

  @override
  SafeBox read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SafeBox(
      number: fields[0] as int,
      name: fields[1] as String,
      defaultWalletAddress: fields[2] as String,
      imagePath: fields[3] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, SafeBox obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.number)
      ..writeByte(1)
      ..write(obj.name)
      ..writeByte(2)
      ..write(obj.defaultWalletAddress)
      ..writeByte(3)
      ..write(obj.imagePath);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SafeBoxAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
