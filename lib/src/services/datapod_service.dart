import 'package:durt2/src/global.dart';
import 'package:durt2/src/models/graphql/accounts.graphql.dart';
import 'package:durt2/src/models/identity_suggestion.dart';
import 'package:graphql/client.dart';

class DatapodService {
  static GraphQLClient? _client;

  static GraphQLClient get client {
    if (_client == null) {
      throw Exception(
          "GraphQLService not initialized. Call SquidService.init() first.");
    }
    return _client!;
  }

  static Future<String> selectFastestEndpoint(List<String> endpoints) async {
    //TODO: finish to implement datapod
    String? fastestUrl;
    int fastestLatency = 99999;

    for (final endpoint in endpoints) {
      final stopwatch = Stopwatch()..start();
      try {
        final parsedEndpoint = endpoint.replaceFirst('wss', 'https');
        final link = HttpLink(parsedEndpoint);
        final client = GraphQLClient(cache: GraphQLCache(), link: link);
        await client.query(QueryOptions(document: gql(r'''
          query {
            block(limit: 1, orderBy: {height: DESC}) {
              height
            }
          }
        ''')));
        final latency = stopwatch.elapsedMilliseconds;
        if (latency < fastestLatency) {
          fastestUrl = endpoint;
          fastestLatency = latency;
        }
        link.dispose();
      } catch (e) {
        log.e('Failed to connect to $endpoint: $e');
      } finally {
        stopwatch.stop();
      }
    }

    if (fastestUrl == null) {
      throw Exception('No GraphQL endpoint available');
    }

    return fastestUrl;
  }

  static Future<void> init(List<String> endpoints) async {
    final squidEndpoint = await DatapodService.selectFastestEndpoint(endpoints);

    _client = GraphQLClient(
      cache: GraphQLCache(),
      link: WebSocketLink(squidEndpoint),
    );
  }
}

extension DurtGraphQLClient on GraphQLClient {
  Future<Query$GetAccountHistory$transferConnection?> getAccountHistory(
      String address,
      {int number = 10,
      String? cursor}) async {
    final variables = Variables$Query$GetAccountHistory(
        address: address, first: number, after: cursor);
    final options = Options$Query$GetAccountHistory(
      variables: variables,
      fetchPolicy: FetchPolicy.networkOnly,
    );

    try {
      final result = await query$GetAccountHistory(options);
      return result.parsedData?.transferConnection;
    } catch (e) {
      log.e('Failed to get account history: $e');
      return null;
    }
  }

  Stream<Subscription$SubAccountHistory$transferConnection$edges$node?>
      subAccountHistory(String address) {
    final variables =
        Variables$Subscription$SubAccountHistory(address: address);
    final options =
        Options$Subscription$SubAccountHistory(variables: variables);

    return subscribe$SubAccountHistory(options)
        .map((event) => event.parsedData?.transferConnection.edges.first.node);
  }

  Future<String?> getIdentityName(String address) async {
    final variables = Variables$Query$GetIdentityName(address: address);
    final options = Options$Query$GetIdentityName(
      variables: variables,
      fetchPolicy: FetchPolicy.networkOnly,
    );
    final result = await query$GetIdentityName(options);
    final identity =
        result.parsedData?.accountConnection.edges.firstOrNull?.node.identity;

    return identity?.name;
  }

  Future<List<IdentitySuggestion>> searchAddressByName(String name) async {
    final variables = Variables$Query$SerachAddressByName(name: '%$name%');
    final options = Options$Query$SerachAddressByName(
      variables: variables,
      fetchPolicy: FetchPolicy.networkOnly,
    );
    final result = await query$SerachAddressByName(options);
    final parsedResult = result.parsedData?.identityConnection.edges;

    if (parsedResult == null) return [];

    final List<IdentitySuggestion> listIdentities = [];
    for (final edge in parsedResult) {
      final identity = IdentitySuggestion(
          name: edge.node.name, address: edge.node.accountId!);
      listIdentities.add(identity);
    }
    return listIdentities;
  }
}
