import 'dart:io';
import 'package:durt2/src/global.dart';
import 'package:durt2/src/models/encrypted_mnemonic.dart';
import 'package:durt2/src/models/safe_box.dart';
import 'package:durt2/src/models/wallet_data.dart';
import 'package:hive/hive.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';

class HiveService {
  static Future<void> init() async {
    final appDocDir = await getApplicationDocumentsDirectory();
    final appDir = Directory(path.join(appDocDir.path, 'durt2'));

    if (!await appDir.exists()) {
      await appDir.create(recursive: true);
    }

    Hive.init(appDir.path);

    Hive.registerAdapter(WalletDataAdapter());
    Hive.registerAdapter(WalletDataKeyAdapter());
    Hive.registerAdapter(SafeBoxAdapter());
    Hive.registerAdapter(EncryptedMnemonicAdapter());
    Hive.registerAdapter(IdtyStatusAdapter());
    // await Hive.deleteBoxFromDisk('walletData');
    // await Hive.deleteBoxFromDisk('safeBox');
    // await Hive.deleteBoxFromDisk('configBox');
    walletDataBox = await Hive.openBox<WalletData>('walletData');
    safeBox = await Hive.openBox<SafeBox>('safeBox');
    configBox = await Hive.openBox('configBox');
  }
}
