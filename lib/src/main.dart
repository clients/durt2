import 'dart:convert' show json;
import 'package:durt2/src/global.dart';
import 'package:durt2/src/models/polkadart_generated/gdev/gdev.dart';
import 'package:durt2/src/services/duniter_service.dart';
import 'package:durt2/src/services/hive_service.dart';
import 'package:durt2/src/models/networks.dart';
import 'package:durt2/src/services/squid_service.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:polkadart/apis/apis.dart' show AuthorApi;
import 'package:polkadart/provider.dart' show Provider;

class Durt {
  static Durt? _instance;
  static Durt get instance => _instance ?? (throw InitException());

  String? _endpoint;
  String get endpoint {
    return _endpoint ?? (throw InitException());
  }

  Gdev? _gdev;
  Gdev get gdev {
    return _gdev ?? (throw InitException());
  }

  Provider? _polkadartProvider;
  Provider get polkadartProvider {
    return _polkadartProvider ?? (throw InitException());
  }

  AuthorApi? _authorApi;
  AuthorApi get authorApi {
    return _authorApi ?? (throw InitException());
  }

  Future<void> init(
      {required Networks network,
      bool initDuniter = true,
      bool initSquid = true}) async {
    late String duniterEndpointsJson;
    late String squidEndpointsJson;

    try {
      duniterEndpointsJson =
          await rootBundle.loadString('config/duniter_endpoints.json');
      log.d('Loading duniter_endpoints.json from local assets');
    } catch (e) {
      duniterEndpointsJson = await rootBundle
          .loadString('packages/durt2/config/duniter_endpoints.json');
      log.d('Loading duniter_endpoints.json from package assets');
    }

    try {
      squidEndpointsJson =
          await rootBundle.loadString('config/squid_endpoints.json');
      log.d('Loading squid_endpoints.json from local assets');
    } catch (e) {
      squidEndpointsJson = await rootBundle
          .loadString('packages/durt2/config/squid_endpoints.json');
      log.d('Loading squid_endpoints.json from package assets');
    }

    final endpointsDuniterMap =
        json.decode(duniterEndpointsJson) as Map<String, dynamic>;
    final endpointsDuniter =
        List<String>.from(endpointsDuniterMap[network.name]);

    final endpointsSquidMap =
        json.decode(squidEndpointsJson) as Map<String, dynamic>;
    final endpointsSquid = List<String>.from(endpointsSquidMap[network.name]);

    await Future.wait([
      HiveService.init(),
      if (initDuniter) _initDuniter(endpointsDuniter),
      if (initSquid) SquidService.init(endpointsSquid),
    ]);
    Durt._instance = this;
  }

  Future<void> _initDuniter(List<String> endpoints) async {
    final duniterService = DuniterService();
    _endpoint = await duniterService.getFastestEndpoint(endpoints);
    log.i('Choosed endpoint: $endpoint');

    _polkadartProvider = Provider.fromUri(Uri.parse(endpoint));
    _authorApi = AuthorApi(polkadartProvider);
    _gdev = Gdev(polkadartProvider);
  }
}

class InitException implements Exception {
  InitException();

  @override
  String toString() => 'You have to call Durt2.init() first';
}
